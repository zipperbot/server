package com.bot.server.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionInterceptor extends HandlerInterceptorAdapter
{
	private static SessionInterceptor singleInstance_ = new SessionInterceptor();
	
	public static SessionInterceptor getInstance()
	{
		return singleInstance_;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, Object handler) throws Exception 
	{
		Session.start(request);
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
			Object handler, Exception ex) throws Exception 
	{
		Session.complete();
	}
}