package com.bot.server.session;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bot.server.dto.Profile;
import com.bot.server.util.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Session
{
	private static ThreadLocal<Session> tl_ = new ThreadLocal<Session>();
	private static ObjectMapper mapper_ = new ObjectMapper();
	private static Logger logger_ = LoggerFactory.getLogger(Session.class);
	
	private Profile profile = new Profile();
	
	private Session()
	{
	}
	
	private Session(HttpServletRequest req) throws Exception
	{
		String hprofile = req.getHeader("x-profile");
		
		if (!Helper.isNullOrEmpty(hprofile))
		{
			if (logger_.isDebugEnabled())
				logger_.debug("Session:hprofile = "+hprofile);
			
			hprofile = new String(Base64.getDecoder().decode(hprofile));
			
			if (logger_.isDebugEnabled())
				logger_.debug("Session:hprofile1 = "+hprofile);
			
			this.profile = mapper_.readValue(hprofile, Profile.class);
		}
	}
	
	static void start(HttpServletRequest req) throws Exception
	{
		tl_.set(new Session(req));
	}
	
	static void complete()
	{
		tl_.remove();
	}
	
	public static Session me()
	{
		return tl_.get();
	}
	
	public static Session ensuredMe()
	{
		Session session = tl_.get();
		
		if (session == null)
			session = new Session();
		
		return session;
	}
	
	public Profile getProfile()
	{
		return this.profile;
	}
}