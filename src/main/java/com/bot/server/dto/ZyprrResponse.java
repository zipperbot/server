package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrResponse<T>
{
	public static final String SUCCESS = "SUCCESS";
	
	private String status;
	private T result;
	
	public String getStatus()
	{
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public T getResult()
	{
		return result;
	}

	public void setResult(T result)
	{
		this.result = result;
	}
}