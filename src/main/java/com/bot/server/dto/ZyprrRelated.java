package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrRelated
{
	private String id;
	private String name;
	private String objectType;
	
	public ZyprrRelated()
	{
	}
	
	ZyprrRelated(Related that)
	{
		this.id = that.getId();
		this.name = that.getName();
		this.objectType = that.getType();
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getObjectType()
	{
		return objectType;
	}

	public void setObjectType(String objectType)
	{
		this.objectType = objectType;
	}
	
	public Related toSelf()
	{
		return new Related(this);
	}
}