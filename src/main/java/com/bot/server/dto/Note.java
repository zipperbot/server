package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Note
{
	private String id;
	private Related related;
	private String note;
	private String createdOn;
	private String updatedOn;
	private Contact contact;
	
	public Note()
	{
	}
	
	Note(ZyprrNote that)
	{
		this.id = that.getId();
		this.note = that.getNotes();
		this.related = that.getRelated() != null ? that.getRelated().toSelf() : null;
		this.createdOn = that.getCreatedTime();
		this.updatedOn = that.getUpdatedTime();
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Related getRelated()
	{
		return related;
	}
	
	public void setRelated(Related related)
	{
		this.related = related;
	}
	
	public Contact getContact()
    {
        return contact;
    }

    public void setContact(Contact contact)
    {
        this.contact = contact;
    }

    public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}
	
	public String getCreatedOn()
	{
		return createdOn;
	}

	public void setCreatedOn(String createdOn)
	{
		this.createdOn = createdOn;
	}

	public String getUpdatedOn()
	{
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn)
	{
		this.updatedOn = updatedOn;
	}

	public ZyprrNote toZyprr()
	{
		return new ZyprrNote(this);
	}
}