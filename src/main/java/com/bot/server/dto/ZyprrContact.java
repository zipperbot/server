package com.bot.server.dto;

import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrContact
{
	private String id;
	private String firstName;
	private String lastName;
	private String emailWork1;
	private String phoneOffice;
	private String email;
	private String phone;
	private String photoUrl;
	private String tPhotoPath;
	
	public ZyprrContact()
	{
	}
	
	ZyprrContact(Contact that)
	{
		this.id = that.getId();
		
		if (!Helper.isNullOrEmpty(that.getName()))
		{
			String [] parts = that.getName().split("\\s+");
			
			if (parts.length > 1)
			{
				this.firstName = parts[0];
				this.lastName = parts[parts.length-1];
			}
			else
			{
				this.lastName = parts[0];
			}
		}
		
		this.email = that.getEmail();
		this.emailWork1 = that.getEmail();
		this.phone = that.getPhone();
		this.phoneOffice = that.getPhone();
		this.photoUrl = that.getImageURL();
		this.tPhotoPath = that.getThumbnail();
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmailWork1()
	{
		return emailWork1;
	}

	public void setEmailWork1(String emailWork1)
	{
		this.emailWork1 = emailWork1;
	}

	public String getPhoneOffice()
	{
		return phoneOffice;
	}

	public void setPhoneOffice(String phoneOffice)
	{
		this.phoneOffice = phoneOffice;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	
	public String getPhotoUrl()
	{
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl)
	{
		this.photoUrl = photoUrl;
	}

	public String gettPhotoPath()
	{
		return tPhotoPath;
	}

	public void settPhotoPath(String tPhotoPath)
	{
		this.tPhotoPath = tPhotoPath;
	}

	public Contact toSelf()
	{
		return new Contact(this);
	}
}