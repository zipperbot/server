package com.bot.server.dto;

import java.util.ArrayList;
import java.util.List;

public class ListResponse<T>
{
	private int count;
	private List<T> data;
	
	public static <T> ListResponse<T> neu(Class<T> type)
	{
		return new ListResponse<T>();
	}
	
	private ListResponse()
	{
	}
	
	public ListResponse<T> data(List<T> data)
	{
		this.data = data;
		return this;
	}
	
	public void addData(T arg)
	{
		if (this.data == null)
			this.data = new ArrayList<T>();
		
		this.data.add(arg);
	}
	
	public ListResponse<T> count(int arg)
	{
		this.count = arg;
		return this;
	}

	public int getCount()
	{
		return count;
	}

	public List<T> getData()
	{
		return data;
	}
}