package com.bot.server.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PropertySearchCrit
{
	private String town;
	private String pt;
	private int sqFt;
	private int beds;
	private float baths;
	private double fromLat;
	private double fromLng;
	private long distance;
	private long price;
	private String operator;
	private String status;
	private String lowDate;
	private String highDate;
	
	public static PropertySearchCrit neu()
	{
		return new PropertySearchCrit();
	}
	
	public PropertySearchCrit town(String arg)
	{
		this.town = arg;
		return this;
	}
	
	public String town()
	{
		return this.town;
	}
	
	public PropertySearchCrit sqFt(Integer arg)
	{
		if (arg != null)
			this.sqFt = arg;
		
		return this;
	}
	
	public int sqFt()
	{
		return this.sqFt;
	}
	
	public PropertySearchCrit beds(Integer arg)
	{
		if (arg != null)
			this.beds = arg;
		
		return this;
	}
	
	public int beds()
	{
		return this.beds;
	}
	
	public PropertySearchCrit baths(Float arg)
	{
		if (arg != null)
			this.baths = arg;
		
		return this;
	}
	
	public float baths()
	{
		return this.baths;
	}
	
	public PropertySearchCrit fromLat(Double arg)
	{
		if (arg != null)
			this.fromLat = arg;
		
		return this;
	}
	
	public double fromLat()
	{
		return this.fromLat;
	}
	
	public PropertySearchCrit fromLng(Double arg)
	{
		if (arg != null)
			this.fromLng = arg;
		
		return this;
	}
	
	public double fromLng()
	{
		return this.fromLng;
	}
	
	public PropertySearchCrit distance(Long arg)
	{
		if (arg != null)
			this.distance = arg;
		
		return this;
	}
	
	public PropertySearchCrit pt(String arg)
	{
		this.pt = arg;
		return this;
	}
	
	public PropertySearchCrit st(String arg)
	{
		this.status = arg;
		return this;
	}
	
	private String toDate(String dt)
	{
		Date retVal = null;
		
		if (dt != null)
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			try
			{
				retVal = df.parse(dt);
			}
			catch (Exception e)
			{
			}
		}
		
		return retVal != null ? new SimpleDateFormat("MM/dd/yyyy").format(retVal) : null;
	}
	
	public PropertySearchCrit dt(String arg)
	{
		if (arg != null && arg.trim().length() > 0)
		{
			String [] dates = arg.split("\\s*/\\s*");
			
			if (dates.length > 0)
				this.lowDate = toDate(dates[0]);
			if (dates.length > 1)
				this.highDate = toDate(dates[1]);
		}
		
		return this;
	}
	
	public String pt()
	{
		return this.pt;
	}
	
	public PropertySearchCrit op(String arg)
	{
		this.operator = arg;
		return this;
	}
	
	public String op()
	{
		return this.operator;
	}
	
	public PropertySearchCrit price(Long arg)
	{
		if (arg != null)
			this.price = arg.longValue();
		
		return this;
	}
	
	public long price()
	{
		return this.price;
	}
	
	public long distance()
	{
		return this.distance;
	}
	
	public String status()
	{
		return this.status;
	}
	
	public String lowDate()
	{
		return this.lowDate;
	}
	
	public String highDate()
	{
		return this.highDate;
	}
	
	public void done()
	{
		if (this.status == null)
			this.status = "ACT,NEW,PCG,RAC,BOM,EXT";
	}
	
	@Override
	public String toString()
	{
		return "PropertySearchCrit [town=" + town + ", pt=" + pt + ", sqFt="
				+ sqFt + ", beds=" + beds + ", baths=" + baths + ", fromLat="
				+ fromLat + ", fromLng=" + fromLng + ", distance=" + distance
				+ ", price=" + price + ", operator=" + operator + "]";
	}

	public boolean isEmpty()
	{
		return PropertySearchCrit.neu().toString().equals(this.toString());
	}
}