package com.bot.server.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bot.server.session.Session;
import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrActivity
{
	private static Logger logger_ = LoggerFactory.getLogger(ZyprrActivity.class);
	public static enum Status
	{
		NSTRTD
		, IPRGRS
		, WOSE
		, DFRRED
		, CNCLD
		, CMPLTD
		;
		
		public static Status toStatus(Activity.Status that)
		{
			Status retVal = null;
			
			if (that == Activity.Status.NOT_STARTED)
				retVal = Status.NSTRTD;
			else if (that == Activity.Status.INPROGRESS)
				retVal = Status.IPRGRS;
			else if (that == Activity.Status.COMPLETE)
				retVal = Status.CMPLTD;
			else if (that == Activity.Status.CANCELLED)
				retVal = Status.CNCLD;
			
			return retVal;
		}
	};
	
	private String id;
	private int version;
	private String activityType;
	private Status activityStatus;
	private String subject;
	private String start;
	private String end;
	private boolean allDayEvent;
	private ZyprrRelated related;
	
	ZyprrActivity(Activity that)
	{
		this.id = that.getId();
		this.version = that.getVersion();
		this.activityType = that.getType().name();
		this.subject = that.getSubject();
		this.allDayEvent = that.isAllDay();
		this.related = that.getRelated() != null ? that.getRelated().toZyprr() : null;
		
		if (that.getStatus() != null)
		{
			if (Activity.Status.NOT_STARTED == that.getStatus())
				this.activityStatus = Status.NSTRTD;
			else if (Activity.Status.INPROGRESS == that.getStatus())
				this.activityStatus = Status.IPRGRS;
			else if (Activity.Status.COMPLETE == that.getStatus())
				this.activityStatus = Status.CMPLTD;
			else if (Activity.Status.CANCELLED == that.getStatus())
				this.activityStatus = Status.CNCLD;
		}
		
		Profile profile = Session.me().getProfile();
		
		try
		{
			if (this.allDayEvent)
			{
				DateFormat df = new SimpleDateFormat(profile.getDateFormat());
				this.start = df.format(Helper.parseDate(that.getStart()));
			}
			else
			{
				DateFormat df = new SimpleDateFormat(profile.getDateFormat()+" "+profile.getTimeFormat());
				Date sd = Helper.parse(that.getStart());
				this.start = df.format(sd);
				this.end = df.format(new Date(sd.getTime()+that.getDuration()));
			}
		}
		catch (Exception e)
		{
			logger_.error("Error while parsing date", e);
		}
	}
	
	public ZyprrActivity()
	{
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}

	public String getActivityType()
	{
		return activityType;
	}

	public void setActivityType(String type)
	{
		this.activityType = type;
	}

	public Status getActivityStatus()
	{
		return activityStatus;
	}

	public void setActivityStatus(Status activityStatus)
	{
		this.activityStatus = activityStatus;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getStart()
	{
		return start;
	}

	public void setStart(String start)
	{
		this.start = start;
	}

	public String getEnd()
	{
		return end;
	}

	public void setEnd(String end)
	{
		this.end = end;
	}

	public boolean isAllDayEvent()
	{
		return allDayEvent;
	}

	public void setAllDayEvent(boolean allDayEvent)
	{
		this.allDayEvent = allDayEvent;
	}

	public ZyprrRelated getRelated()
	{
		return related;
	}

	public void setRelated(ZyprrRelated related)
	{
		this.related = related;
	}
	
	public Activity toSelf()
	{
		return new Activity(this);
	}
}