package com.bot.server.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bot.server.service.system.Target;
import com.bot.server.service.system.ZAArea;
import com.bot.server.service.system.ZACounty;
import com.bot.server.service.system.ZATown;
import com.bot.server.service.system.ZAuth;
import com.bot.server.service.system.ZProfile;

public class ZAProfile extends Profile
{
	public ZAProfile()
	{
		super();
	}
	
	public ZAProfile(Target target, ZAuth auth, ZProfile that)
	{
		super(target, auth, that);
	}
	
	private Map<String, Object> settings;
	private List<ZATown> towns;
	private List<ZACounty> counties;
	private List<ZAArea> areas;
	
	public void add(ZATown town)
	{
		if (towns == null)
			towns = new ArrayList<ZATown>();
		
		towns.add(town);
	}
	
	public void add(ZAArea area)
	{
		if (areas == null)
			areas = new ArrayList<ZAArea>();
		
		areas.add(area);
	}
	
	public void add(ZACounty county)
	{
		if (counties == null)
			counties = new ArrayList<ZACounty>();
		
		counties.add(county);
	}
	
	public ZAProfile settings(Map<String, Object> settings)
	{
		this.settings = settings;
		return this;
	}
	
	public List<ZATown> getTowns()
	{
		return towns;
	}

	public List<ZACounty> getCounties()
	{
		return counties;
	}

	public List<ZAArea> getAreas()
	{
		return areas;
	}
	
	public Map<String, Object> getSettings()
	{
		return settings;
	}
}