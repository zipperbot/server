package com.bot.server.dto;

import com.bot.server.service.system.Target;
import com.bot.server.service.system.ZAuth;
import com.bot.server.service.system.ZProfile;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Profile
{
	private Target target;
	private String token;
	private String handle = "ANONYMOUS";
	private String firstName;
	private String lastName;
	private String email;
	private String role;
	private String tenant;
	private String timezone;
	private String dateFormat;
	private String timeFormat;
	
	public Profile()
	{
	}
	
	public Profile(Target target, ZAuth auth, ZProfile that)
	{
		this.target = target;
		this.token = auth.getAccessToken();
		this.handle = that.getHandle();
		this.firstName = that.getFirstName();
		this.lastName = that.getLastName();
		this.email = that.getEmail();
		this.role = that.getRole();
		this.tenant = auth.getTenant();
		
		if (auth.getSettings() != null)
		{
			this.timezone = auth.getSettings().getTimezone();
			this.dateFormat = auth.getSettings().getDateFormat();
			this.timeFormat = auth.getSettings().getTimeFormat();
		}
	}
	
	public Target getTarget()
	{
		return target;
	}

	public String getToken()
	{
		return token;
	}

	public String getHandle()
	{
		return handle;
	}
	
	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getEmail()
	{
		return email;
	}
	
	public String getRole()
	{
		return role;
	}

	public String getTenant()
	{
		return tenant;
	}
	
	public String getTimezone()
	{
		return timezone;
	}

	public String getDateFormat()
	{
		return dateFormat;
	}

	public String getTimeFormat()
	{
		return timeFormat;
	}

	@Override
	public String toString()
	{
		return "Profile [target=" + target + ", token=" + token + ", handle="
				+ handle + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", role=" + role
				+ ", tenant=" + tenant + ", timezone=" + timezone
				+ ", dateFormat=" + dateFormat + ", timeFormat=" + timeFormat
				+ "]";
	}
}