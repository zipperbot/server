package com.bot.server.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZyprrBulkRequest
{
	private List<Map<String, Object>> fields = new ArrayList<Map<String, Object>>();
	private List<Map<String, Object>> ids = new ArrayList<Map<String, Object>>();
	
	public static ZyprrBulkRequest newInstance()
	{
		return new ZyprrBulkRequest();
	}
	
	public ZyprrBulkRequest addField(String name, Object value)
	{
		Map<String, Object> field = new HashMap<String, Object>();
		field.put("field", name);
		field.put("value", value);
		this.fields.add(field);
		return this;
	}
	
	public ZyprrBulkRequest addId(String id, int version)
	{
		Map<String, Object> field = new HashMap<String, Object>();
		field.put("id", id);
		field.put("version", version);
		this.ids.add(field);
		return this;
	}

	public List<Map<String, Object>> getFields()
	{
		return fields;
	}

	public List<Map<String, Object>> getIds()
	{
		return ids;
	}
}