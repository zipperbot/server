package com.bot.server.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZAProperty
{
	private String id;
	private double lat;
	private double lng;
	private String lngTOWNSDESCRIPTION;
	private String remarks;
	private List<Photo> photoList;
	private String streetname;
	private String streetno;
	private String zipcode;
	private String unitno;
	private String provinceState;
	private float nobaths;
	private int nobedrooms;
	private double squarefeet;
	private double acre;
	private double listprice;
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public double getLat()
	{
		return lat;
	}

	public void setLat(double lat)
	{
		this.lat = lat;
	}

	public double getLng()
	{
		return lng;
	}

	public void setLng(double lng)
	{
		this.lng = lng;
	}

	public String getLngTOWNSDESCRIPTION()
	{
		return lngTOWNSDESCRIPTION;
	}

	public void setLngTOWNSDESCRIPTION(String lngTOWNSDESCRIPTION)
	{
		this.lngTOWNSDESCRIPTION = lngTOWNSDESCRIPTION;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public List<Photo> getPhotoList()
	{
		return photoList;
	}

	public void setPhotoList(List<Photo> photoList)
	{
		this.photoList = photoList;
	}

	public String getStreetname()
	{
		return streetname;
	}

	public void setStreetname(String streetname)
	{
		this.streetname = streetname;
	}

	public String getStreetno()
	{
		return streetno;
	}

	public void setStreetno(String streetno)
	{
		this.streetno = streetno;
	}

	public String getZipcode()
	{
		return zipcode;
	}

	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	public String getUnitno()
	{
		return unitno;
	}

	public void setUnitno(String unitno)
	{
		this.unitno = unitno;
	}

	public String getProvinceState()
	{
		return provinceState;
	}

	public void setProvinceState(String provinceState)
	{
		this.provinceState = provinceState;
	}

	public float getNobaths()
	{
		return nobaths;
	}

	public void setNobaths(float nobaths)
	{
		this.nobaths = nobaths;
	}

	public int getNobedrooms()
	{
		return nobedrooms;
	}

	public void setNobedrooms(int nobedrooms)
	{
		this.nobedrooms = nobedrooms;
	}
	
	public double getSquarefeet()
	{
		return squarefeet;
	}

	public void setSquarefeet(double squarefeet)
	{
		this.squarefeet = squarefeet;
	}

	public double getAcre()
	{
		return acre;
	}

	public void setAcre(double acre)
	{
		this.acre = acre;
	}

	public double getListprice()
	{
		return listprice;
	}

	public void setListprice(double listprice)
	{
		this.listprice = listprice;
	}

	public Property toSelf()
	{
		return new Property(this);
	}
	
	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Photo
	{
		private String imgurl;

		public String getImgurl()
		{
			return imgurl;
		}

		public void setImgurl(String imgurl)
		{
			this.imgurl = imgurl;
		}
	}
}
