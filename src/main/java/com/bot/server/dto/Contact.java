package com.bot.server.dto;

import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Contact
{
	private String id;
	private String name;
	private String email;
	private String phone;
	private String thumbnail;
	private String imageURL;
	
	public Contact()
	{
	}
	
	Contact(ZyprrContact that)
	{
		this.id = that.getId();
		StringBuffer sb = new StringBuffer();
		
		if (!Helper.isNullOrEmpty(that.getFirstName()))
			sb.append(that.getFirstName());
		
		if (!Helper.isNullOrEmpty(that.getLastName()))
			sb.append(sb.length() < 1 ? "" : " ").append(that.getLastName());
		
		this.name = sb.toString();
		this.email = that.getEmail();
		
		if (Helper.isNullOrEmpty(this.email))
			this.email = that.getEmailWork1();
		
		this.phone = that.getPhone();
		
		if (Helper.isNullOrEmpty(this.phone))
			this.phone = that.getPhoneOffice();
		
		this.thumbnail = that.gettPhotoPath();
		this.imageURL = that.getPhotoUrl();
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	
	public String getThumbnail()
	{
		return thumbnail;
	}

	public void setThumbnail(String thumbnail)
	{
		this.thumbnail = thumbnail;
	}

	public String getImageURL()
	{
		return imageURL;
	}

	public void setImageURL(String imageURL)
	{
		this.imageURL = imageURL;
	}

	public ZyprrContact toZyprr()
	{
		return new ZyprrContact(this);
	}
}