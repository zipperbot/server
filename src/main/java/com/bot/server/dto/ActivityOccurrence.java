package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ActivityOccurrence
{
	private String id;
	private int version;
	private String start;
	private boolean allDay;
	private String end;
	private Activity activity;
	
	public ActivityOccurrence()
	{
	}
	
	ActivityOccurrence(ZyprrActivityOccurrence that)
	{
		this.id = that.getId();
		this.version = that.getVersion();
		this.allDay = that.isAllDayEvent();
		this.start = that.getStart();
		this.end = that.getEnd();
		this.activity = that.getActivity() != null ? that.getActivity().toSelf() : null;
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}

	public String getStart()
	{
		return start;
	}

	public void setStart(String start)
	{
		this.start = start;
	}

	public boolean isAllDay()
	{
		return allDay;
	}

	public void setAllDay(boolean allDay)
	{
		this.allDay = allDay;
	}

	public String getEnd()
	{
		return end;
	}

	public void setEnd(String end)
	{
		this.end = end;
	}

	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public ZyprrActivityOccurrence toZyprr()
	{
		return new ZyprrActivityOccurrence(this);
	}
}