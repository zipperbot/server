package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrNote
{
	private String id;
	private String notes;
	private String createdTime;
	private String updatedTime;
	private ZyprrRelated related;
	private ZyprrContact contact;
	
	public ZyprrNote()
	{
	}
	
	ZyprrNote(Note that)
	{
		this.id = that.getId();
		this.notes = that.getNote();
		this.related = that.getRelated() != null ? that.getRelated().toZyprr() : null;
		this.contact = that.getContact() != null ? that.getContact().toZyprr() : null;
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getNotes()
	{
		return notes;
	}
	
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	
	public ZyprrRelated getRelated()
	{
		return related;
	}

	public void setRelated(ZyprrRelated related)
	{
		this.related = related;
	}
	
	public ZyprrContact getContact()
    {
        return contact;
    }

    public void setContact(ZyprrContact contact)
    {
        this.contact = contact;
    }

    public String getCreatedTime()
	{
		return createdTime;
	}

	public void setCreatedTime(String createdTime)
	{
		this.createdTime = createdTime;
	}

	public String getUpdatedTime()
	{
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime)
	{
		this.updatedTime = updatedTime;
	}

	public Note toSelf()
	{
		return new Note(this);
	}
}