package com.bot.server.dto;

public class Credential
{
	private String user;
	private String password;
	private boolean extra;
	
	public String getUser()
	{
		return user;
	}
	
	public void setUser(String user)
	{
		this.user = user;
	}
	
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public boolean isExtra()
	{
		return extra;
	}

	public void setExtra(boolean extra)
	{
		this.extra = extra;
	}

	@Override
	public String toString()
	{
		return "Credential [user=" + user + ", password=" + password
				+ ", extra=" + extra + "]";
	}
}