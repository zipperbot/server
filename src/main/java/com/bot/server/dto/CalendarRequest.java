package com.bot.server.dto;

public class CalendarRequest
{
	private String subject;
	private String startDate;
	private String startTime = "09:00:00";
	private String target = "google";
	private long duration = 1800000l;
	
	public String getSubject()
	{
		return subject;
	}
	
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	
	public String getStartDate()
	{
		return startDate;
	}
	
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	
	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	public String getTarget()
	{
		return target;
	}
	
	public void setTarget(String target)
	{
		this.target = target;
	}
	
	public long getDuration()
	{
		return duration;
	}
	
	public void setDuration(long duration)
	{
		this.duration = duration;
	}

	@Override
	public String toString()
	{
		return "CalendarRequest [subject=" + subject + ", startDate="
				+ startDate + ", startTime=" + startTime + ", target=" + target
				+ ", duration=" + duration + "]";
	}
}