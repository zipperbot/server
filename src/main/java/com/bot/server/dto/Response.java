package com.bot.server.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Response
{
    private int status = 200;
    private Object result;
    
    public static Response OK(Object result)
    {
        return new Response(result);
    }
    
    public static Response OK()
    {
        return new Response();
    }
    
    private Response()
    {
    }
    
    private Response(Object result)
    {
        this.result = result;
    }

    public int getStatus()
    {
        return status;
    }

    public Object getResult()
    {
        return result;
    }
}