package com.bot.server.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrActivityOccurrence
{
	private String id;
	private int version;
	private String start;
	private String end;
	private boolean allDayEvent;
	private ZyprrActivity activity;
	
	ZyprrActivityOccurrence(ActivityOccurrence that)
	{
		this.id = that.getId();
		this.version = that.getVersion();
		this.allDayEvent = that.isAllDay();
		this.start = that.getStart();
		this.end = that.getEnd();
		this.activity = that.getActivity() != null ? that.getActivity().toZyprr() : null;
	}
	
	public ZyprrActivityOccurrence()
	{
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}

	public String getStart()
	{
		return start;
	}

	public void setStart(String start)
	{
		this.start = start;
	}

	public String getEnd()
	{
		return end;
	}

	public void setEnd(String end)
	{
		this.end = end;
	}

	public boolean isAllDayEvent()
	{
		return allDayEvent;
	}

	public void setAllDayEvent(boolean allDayEvent)
	{
		this.allDayEvent = allDayEvent;
	}

	public ZyprrActivity getActivity()
	{
		return activity;
	}

	public void setActivity(ZyprrActivity activity)
	{
		this.activity = activity;
	}

	public ActivityOccurrence toSelf()
	{
		return new ActivityOccurrence(this);
	}
}