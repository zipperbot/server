package com.bot.server.dto;

import java.util.ArrayList;
import java.util.List;

import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Property
{
	private String id;
	private String streetAddress;
	private String state;
	private String town;
	private String zip;
	private String remarks;
	private List<String> photos;
	private double lat;
	private double lng;
	private int beds;
	private float baths;
	private double price;
	private double squareFeet;
	private double acre;
	
	public Property(ZAProperty that)
	{
		this.id = that.getId();
		this.state = that.getProvinceState();
		this.town = that.getLngTOWNSDESCRIPTION();
		this.zip = that.getZipcode();
		this.remarks = that.getRemarks();
		this.lat = that.getLat();
		this.lng = that.getLng();
		this.beds = that.getNobedrooms();
		this.baths = that.getNobaths();
		this.price = that.getListprice();
		this.squareFeet = that.getSquarefeet();
		this.acre = that.getAcre();
		
		StringBuffer sb = new StringBuffer();
		
		if (!Helper.isNullOrEmpty(that.getStreetno()))
			sb.append(sb.length() > 0 ? "," : "").append(that.getStreetno());
		
		if (!Helper.isNullOrEmpty(that.getStreetname()))
			sb.append(sb.length() > 0 ? "," : "").append(that.getStreetname());
		
		if (!Helper.isNullOrEmpty(that.getUnitno()))
			sb.append(sb.length() > 0 ? "," : "").append(that.getUnitno());
		
		this.streetAddress = sb.toString();
		
		if (that.getPhotoList() != null && that.getPhotoList().size() > 0)
		{
			this.photos = new ArrayList<String>();
			
			for (ZAProperty.Photo photo : that.getPhotoList())
			{
				if (!Helper.isNullOrEmpty(photo.getImgurl()))
					this.photos.add(photo.getImgurl());
			}
		}
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getStreetAddress()
	{
		return streetAddress;
	}
	
	public String getState()
	{
		return state;
	}
	
	public String getTown()
	{
		return town;
	}
	
	public String getZip()
	{
		return zip;
	}
	
	public String getRemarks()
	{
		return remarks;
	}

	public List<String> getPhotos()
	{
		return photos;
	}
	
	public double getLat()
	{
		return lat;
	}

	public double getLng()
	{
		return lng;
	}

	public int getBeds()
	{
		return beds;
	}

	public float getBaths()
	{
		return baths;
	}

	public double getPrice()
	{
		return price;
	}

	public double getSquareFeet()
	{
		return squareFeet;
	}

	public double getAcre()
	{
		return acre;
	}
}