package com.bot.server.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ZyprrList<T>
{
	private int dataCount;
	private int totalCount;
	private List<T> filteredList;
	
	public int getDataCount()
	{
		return dataCount;
	}
	
	public void setDataCount(int dataCount)
	{
		this.dataCount = dataCount;
	}
	
	public int getTotalCount()
	{
		return totalCount;
	}
	
	public void setTotalCount(int totalCount)
	{
		this.totalCount = totalCount;
	}
	
	public List<T> getFilteredList()
	{
		return filteredList;
	}

	public void setFilteredList(List<T> filteredList)
	{
		this.filteredList = filteredList;
	}
}