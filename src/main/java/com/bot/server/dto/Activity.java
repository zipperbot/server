package com.bot.server.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bot.server.session.Session;
import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Activity
{
	private static Logger logger_ = LoggerFactory.getLogger(Activity.class);
	public static enum Type {TASK, CALL, MEETING};
	public static enum Status {COMPLETE, CANCELLED, NOT_STARTED, INPROGRESS, INTERMEDIATE};
	
	private String id;
	private int version;
	private Type type;
	private Status status;
	private String subject;
	private String start;
	private boolean allDay;
	private long duration;
	private Related related;
	
	public Activity()
	{
	}
	
	Activity(ZyprrActivity that)
	{
		this.id = that.getId();
		this.version = that.getVersion();
		this.type = Type.valueOf(that.getActivityType());
		this.subject = that.getSubject();
		Profile profile = Session.me().getProfile();
		this.allDay = that.isAllDayEvent();
		this.related = that.getRelated() != null ? that.getRelated().toSelf() : null;
		
		if (that.getActivityStatus() != null)
		{
			if (ZyprrActivity.Status.NSTRTD == that.getActivityStatus())
				this.status = Status.NOT_STARTED;
			else if (ZyprrActivity.Status.IPRGRS == that.getActivityStatus())
				this.status = Status.INPROGRESS;
			else if (ZyprrActivity.Status.CMPLTD == that.getActivityStatus())
				this.status = Status.COMPLETE;
			else if (ZyprrActivity.Status.CNCLD == that.getActivityStatus())
				this.status = Status.CANCELLED;
			else
				this.status = Status.INTERMEDIATE;
		}
		
		
		if (this.allDay)
		{
			DateFormat df = new SimpleDateFormat(profile.getDateFormat());
			
			try
			{
				this.start = Helper.formatDate(df.parse(that.getStart()));
			}
			catch (Exception e)
			{
				logger_.error("Error while parsing date", e);
			}
		}
		else
		{
			DateFormat df = new SimpleDateFormat(profile.getDateFormat()+" "+profile.getTimeFormat());
			
			try
			{
				Date sd = df.parse(that.getStart());
				Date ed = df.parse(that.getEnd());
				this.start = Helper.format(sd);
				this.duration = ed.getTime()-sd.getTime();
			}
			catch (Exception e)
			{
				logger_.error("Error while parsing dates", e);
			}
		}
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}

	public Type getType()
	{
		return type;
	}
	
	public void setType(Type type)
	{
		this.type = type;
	}
	
	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public String getSubject()
	{
		return subject;
	}
	
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	
	public String getStart()
	{
		return start;
	}
	
	public void setStart(String start)
	{
		this.start = start;
	}
	
	public boolean isAllDay()
	{
		return allDay;
	}
	
	public void setAllDay(boolean allDay)
	{
		this.allDay = allDay;
	}
	
	public long getDuration()
	{
		return duration;
	}
	
	public void setDuration(long duration)
	{
		this.duration = duration;
	}
	
	public Related getRelated()
	{
		return related;
	}
	
	public void setRelated(Related related)
	{
		this.related = related;
	}
	
	public ZyprrActivity toZyprr()
	{
		return new ZyprrActivity(this);
	}
}