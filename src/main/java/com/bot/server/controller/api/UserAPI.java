package com.bot.server.controller.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bot.server.dto.Credential;
import com.bot.server.dto.Response;
import com.bot.server.service.system.Target;
import com.bot.server.service.system.Targets;

@RestController
@RequestMapping("/api/user")
public class UserAPI
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired Targets targets;
	
    @RequestMapping(value="/login", method=RequestMethod.POST)
    public Response login(@RequestBody Credential req) throws Exception
    {
    	if (logger.isDebugEnabled())
    		logger.debug("UserAPI.login:cred = "+req);
    	
        return Response.OK(targets.get(Target.getDefault()).login(req));
    	//return Response.OK();
    }
    
    @RequestMapping(value="/login/{target}", method=RequestMethod.POST)
    public Response login(@PathVariable("target") Target target, @RequestBody Credential req) throws Exception
    {
        return Response.OK(targets.get(target).login(req));
    }
}