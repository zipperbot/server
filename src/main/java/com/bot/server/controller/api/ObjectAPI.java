package com.bot.server.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bot.server.dto.Activity;
import com.bot.server.dto.Contact;
import com.bot.server.dto.Note;
import com.bot.server.dto.PropertySearchCrit;
import com.bot.server.dto.Response;
import com.bot.server.service.crm.CRMs;
import com.bot.server.service.crm.ObjectKind;
import com.bot.server.service.crm.ObjectType;
import com.bot.server.session.Session;

@RestController
@RequestMapping("/api/bot")
public class ObjectAPI
{
	@Autowired CRMs crms;
	
    @RequestMapping(value="/contact/add", method=RequestMethod.POST)
    public Response addContact(@RequestBody Contact req) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.CONTACT).add(req));
    }
    
    @RequestMapping(value="/contact/list", method=RequestMethod.GET)
    public Response listContact(@RequestParam(value="t", required=false) String t, 
    		@RequestParam("sidx") int sidx, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), 
        		ObjectType.CONTACT).list(t, sidx, ps));
    }
    
    @RequestMapping(value="/contact/similar", method=RequestMethod.GET)
    public Response similarContact(@RequestParam("name") String name, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), 
                ObjectType.CONTACT).similarContact(name, ps));
    }
    
    @RequestMapping(value="/contact/{kind}/list", method=RequestMethod.GET)
    public Response listKindContact(@PathVariable("kind") String kind, 
    		@RequestParam("sidx") int sidx, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), 
        		ObjectType.CONTACT).listContact(sidx, ps, ObjectKind.get(kind)));
    }
    
    @RequestMapping(value="/note/add", method=RequestMethod.POST)
    public Response addNote(@RequestBody Note req) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.NOTE).add(req));
    }
    
    @RequestMapping(value="/note/list", method=RequestMethod.GET)
    public Response listNote(@RequestParam(value="pid", required=false) String pid
    		, @RequestParam(value="ptyp", required=false) String ptype
    		, @RequestParam("sidx") int sidx
    		, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.NOTE)
        		.listNote(pid, ptype, sidx, ps));
    }
    
    @RequestMapping(value="/note/unrelatedList", method=RequestMethod.GET)
    public Response listNote(@RequestParam("sidx") int sidx, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.NOTE)
        		.listUnrelatedNote(sidx, ps));
    }
    
    @RequestMapping(value="/activity/add", method=RequestMethod.POST)
    public Response addNote(@RequestBody Activity req) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), 
        		ObjectType.get(req.getType().name())).add(req));
    }
    
    @RequestMapping(value="/activity/list/{type}", method=RequestMethod.GET)
    public Response listActivity(@PathVariable("type") String type, 
    		@RequestParam("pid") String pid, @RequestParam("ptyp") String ptype, 
    		@RequestParam("sidx") int sidx, @RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.get(type))
        		.listActivity(pid, ptype, type, sidx, ps));
    }
    
    @RequestMapping(value="/activity/list/{what}/{when}", method=RequestMethod.GET)
    public Response listActivity(@PathVariable("what") String what, 
    		@PathVariable("when") String when, 
    		@RequestParam("sidx") int sidx, 
    		@RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.get(what))
        		.listActivity(what, when, sidx, ps));
    }
    
    @RequestMapping(value="/event/list/{what}", method=RequestMethod.GET)
    public Response listActivity(@PathVariable("what") String what, 
    		@RequestParam("start") String start, 
    		@RequestParam("end") String end, 
    		@RequestParam(value="my", required=false) Boolean my, 
    		@RequestParam("sidx") int sidx, 
    		@RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.get(what))
        		.listActivity(what, Boolean.TRUE.equals(my), start, end, sidx, ps));
    }
    
    @RequestMapping(value="/activity/mark/{id}/{version}/{type}", method=RequestMethod.POST)
    public Response markActivity(@PathVariable("id") String id,
    		@PathVariable("version") int version,
    		@PathVariable("type") String type, 
    		@RequestParam("status") Activity.Status status) throws Exception
    {
    	crms.get(Session.me().getProfile().getTarget(), ObjectType.ACTIVITY)
    		.markActivity(id, version, type, status);
        return Response.OK();
    }
    
    @RequestMapping(value="/property/list", method=RequestMethod.GET)
    public Response listProperty( 
    		@RequestParam(value="town", required=false) String town, 
    		@RequestParam(value="sqFt", required=false) Integer sqFt, 
    		@RequestParam(value="beds", required=false) Integer beds, 
    		@RequestParam(value="baths", required=false) Float baths, 
    		@RequestParam(value="pt", required=false) String pt, 
    		@RequestParam(value="op", required=false) String op, 
    		@RequestParam(value="st", required=false) String status, 
    		@RequestParam(value="udr", required=false) String dateRange, 
    		@RequestParam(value="ud", required=false) String dateTime, 
    		@RequestParam(value="price", required=false) Long price, 
    		@RequestParam("sidx") int sidx, 
    		@RequestParam("ps") int ps) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.ESTATE)
        		.listProperties(PropertySearchCrit.neu().baths(baths)
        				.beds(beds).sqFt(sqFt).town(town)
        				.pt(pt).price(price).op(op).st(status).dt(dateTime).dt(dateRange), sidx, ps));
    }
    
    @RequestMapping(value="/property/distance", method=RequestMethod.GET)
    public Response listProperty( 
    		@RequestParam(value="pt", required=false) String pt, 
    		@RequestParam(value="op", required=false) String op, 
    		@RequestParam(value="price", required=false) Long price, 
    		@RequestParam(value="distance", required=false) Double distance, 
    		@RequestParam("lat") double lat, 
    		@RequestParam("lng") double lng, 
    		@RequestParam("sidx") int sidx, 
    		@RequestParam("ps") int ps) throws Exception
    {
    	if (distance == null)
    		distance = 1000d;
    	
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), ObjectType.ESTATE)
        		.listProperties(PropertySearchCrit.neu().pt(pt)
        				.price(price).op(op), lat, lng, distance, sidx, ps));
    }
    
    @RequestMapping(value="/property/address", method=RequestMethod.GET)
    public Response listPropertyAddress( 
    		@RequestParam(value="value") String value) throws Exception
    {
        return Response.OK(crms.get(Session.me().getProfile().getTarget(), 
        		ObjectType.ESTATE).listPropertiesWithAddress(value));
    }
}