package com.bot.server.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bot.server.dto.Response;
import com.bot.server.service.bot.Bots;
import com.bot.server.util.Log;

@RestController
@RequestMapping("/api/bot")
public class BotAPI
{
    @Autowired Bots bots;
    
    @RequestMapping(value="/log/{mode}", method=RequestMethod.POST, consumes="text/plain")
    public Response log(@PathVariable("mode") Log mode, @RequestBody String message) throws Exception
    {
    	if (mode.isEnabled())
    		mode.log(message);
    	
    	return Response.OK();
    }
    
    @RequestMapping("/hi")
    public Response hi() throws Exception
    {
        return Response.OK(bots.curr().hi());
    }
    
    @RequestMapping("/query")
    public Response query(@RequestParam("q") String q, 
            @RequestParam(value="s", required=false) String sessionId) throws Exception
    {
        return Response.OK(bots.curr().query(q, sessionId));
    }
}