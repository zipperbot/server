package com.bot.server.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bot.server.dto.CalendarRequest;
import com.bot.server.dto.Response;
import com.bot.server.service.calendar.Calendars;

@RestController
@RequestMapping("/api/bot/calendar")
public class CalendarAPI
{
	@Autowired Calendars calendars;
	
    @RequestMapping(value="/add", method=RequestMethod.POST)
    public Response add(@RequestBody CalendarRequest req) throws Exception
    {
    	calendars.get(req.getTarget()).add(req);
        return Response.OK();
    }
}