package com.bot.server.service.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZACounty
{
	@JsonProperty("lng")
	private String lng;
	@JsonProperty("shrt")
	private String shrt;
	
	public String getLng()
	{
		return lng;
	}
	
	public void setLng(String lng)
	{
		this.lng = lng;
	}
	
	public String getShrt()
	{
		return shrt;
	}

	public void setShrt(String shrt)
	{
		this.shrt = shrt;
	}
}