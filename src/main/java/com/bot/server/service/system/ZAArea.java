package com.bot.server.service.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZAArea
{
	@JsonProperty("town")
	private int town;
	@JsonProperty("lng")
	private String lng;
	@JsonProperty("shrt")
	private String shrt;
	
	public int getTown()
	{
		return town;
	}
	
	public void setTown(int town)
	{
		this.town = town;
	}
	
	public String getLng()
	{
		return lng;
	}
	
	public void setLng(String lng)
	{
		this.lng = lng;
	}
	
	public String getShrt()
	{
		return shrt;
	}

	public void setShrt(String shrt)
	{
		this.shrt = shrt;
	}
}