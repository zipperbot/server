package com.bot.server.service.system;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZATownAreaCounty
{
	@JsonProperty("towns")
	private List<CodeValue<ZATown>> towns;
	
	@JsonProperty("counties")
	private List<CodeValue<ZACounty>> counties;
	
	@JsonProperty("areas")
	private List<CodeValue<ZAArea>> areas;

	public List<CodeValue<ZATown>> getTowns()
	{
		return towns;
	}

	public void setTowns(List<CodeValue<ZATown>> towns)
	{
		this.towns = towns;
	}

	public List<CodeValue<ZACounty>> getCounties()
	{
		return counties;
	}

	public void setCounties(List<CodeValue<ZACounty>> counties)
	{
		this.counties = counties;
	}

	public List<CodeValue<ZAArea>> getAreas()
	{
		return areas;
	}

	public void setAreas(List<CodeValue<ZAArea>> areas)
	{
		this.areas = areas;
	}
}