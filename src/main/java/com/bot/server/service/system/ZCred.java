package com.bot.server.service.system;

import com.bot.server.dto.Credential;

public class ZCred
{
	private String userName;
	private String password;
	
	ZCred(Credential that)
	{
		this.userName = that.getUser();
		this.password = that.getPassword();
	}

	public String getUserName()
	{
		return userName;
	}

	public String getPassword()
	{
		return password;
	}
}