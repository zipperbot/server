package com.bot.server.service.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZProfile
{
	@JsonProperty("firstName")
	private String firstName;
	@JsonProperty("lastName")
	private String lastName;
	@JsonProperty("login")
	private String handle;
	@JsonProperty("email")
	private String email;
	@JsonProperty("role")
	private String role;
	
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getHandle()
	{
		return handle;
	}
	
	public String getEmail()
	{
		return email;
	}

	public String getRole()
	{
		return role;
	}

	public void setHandle(String handle)
	{
		this.handle = handle;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public void setRole(String role)
	{
		this.role = role;
	}
}