package com.bot.server.service.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZATown
{
	@JsonProperty("number")
	private int number;
	@JsonProperty("lng")
	private String lng;
	@JsonProperty("county")
	private String county;
	@JsonProperty("provinceState")
	private String provinceState;
	@JsonProperty("shrt")
	private String shrt;
	
	public int getNumber()
	{
		return number;
	}
	
	public void setNumber(int number)
	{
		this.number = number;
	}
	
	public String getLng()
	{
		return lng;
	}
	
	public void setLng(String lng)
	{
		this.lng = lng;
	}
	
	public String getCounty()
	{
		return county;
	}
	
	public void setCounty(String county)
	{
		this.county = county;
	}
	
	public String getProvinceState()
	{
		return provinceState;
	}
	
	public void setProvinceState(String provinceState)
	{
		this.provinceState = provinceState;
	}
	
	public String getShrt()
	{
		return shrt;
	}
	
	public void setShrt(String shrt)
	{
		this.shrt = shrt;
	}
}