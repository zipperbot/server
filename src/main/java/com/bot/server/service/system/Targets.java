package com.bot.server.service.system;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Targets
{
	private Map<Target, TargetSystemServiceI> serviceMap = new HashMap<Target, TargetSystemServiceI>();
	
	void register(TargetSystemServiceI service)
	{
		this.serviceMap.put(service.getTarget(), service);
	}
	
	public TargetSystemServiceI get(Target target)
	{
		return this.serviceMap.get(target);
	}
}