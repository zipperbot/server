package com.bot.server.service.system;

import com.bot.server.dto.Credential;
import com.bot.server.dto.Profile;

public interface TargetSystemServiceI
{
	public String getScheme();
	
	public Target getTarget();
	
	public Profile login(Credential cred) throws Exception;
}