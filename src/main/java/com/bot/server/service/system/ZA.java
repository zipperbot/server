package com.bot.server.service.system;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bot.server.dto.Credential;
import com.bot.server.dto.Profile;
import com.bot.server.dto.ZAProfile;
import com.bot.server.dto.ZyprrResponse;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Component
class ZA implements TargetSystemServiceI
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired Targets targets;
	
	@Value("${ext.za.apiKey}")
	private String apiKey;
	@Value("${ext.za.login}")
	private String loginURL;
	@Value("${ext.za.profile}")
	private String profileURL;
	@Value("${ext.za.https}")
	private boolean https;
	@Value("${ext.za.town}")
	private String town;
	@Value("${ext.za.settings}")
	private String settings;
	
	@Override
	public String getScheme()
	{
		return this.https ? "https://" : "http://";
	}
	
    @PostConstruct
    public void done()
    {
        if (logger.isDebugEnabled())
            logger.debug("ZyprrCRM post construct done");
        
        targets.register(this);
    }

	@Override
	public Target getTarget()
	{
		return Target.ZA;
	}

	@Override
	public Profile login(Credential cred) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(this.loginURL);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey)
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, new ZCred(cred));
		
		ZyprrResponse<ZAuth> resp1 = resp.getEntity(new GenericType<ZyprrResponse<ZAuth>>(){});
		ZAuth auth = resp1.getResult();
		resource = client.resource(getScheme()+auth.getTenant()+this.profileURL);
		
		resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey+",access_token="
						+auth.getAccessToken())
							.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZProfile> resp2 = resp.getEntity(new GenericType<ZyprrResponse<ZProfile>>(){});
		ZProfile zProfile = resp2.getResult();
		ZAProfile profile = new ZAProfile(Target.ZA, auth, zProfile);
		
		if (logger.isDebugEnabled())
			logger.debug("ZA:login:profile = "+profile);
		
		resource = client.resource(getScheme()+auth.getTenant()+this.settings);
		
		resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey+",access_token="
						+auth.getAccessToken())
							.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<Map<String, Object>> resp4 = resp.getEntity(
				new GenericType<ZyprrResponse<Map<String, Object>>>(){});
		
		profile.settings(resp4.getResult());
		
		if (cred.isExtra())
		{
			resource = client.resource(getScheme()+auth.getTenant()+this.town);
			
			resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
					"Authorization", "consumer_key="+this.apiKey+",access_token="
							+auth.getAccessToken())
								.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			
			ZyprrResponse<ZATownAreaCounty> resp3 = resp.getEntity(new GenericType<ZyprrResponse<ZATownAreaCounty>>(){});
			
			if (resp3 != null)
			{
				ZATownAreaCounty tac = resp3.getResult();
				
				if (tac.getTowns() != null && tac.getTowns().size() > 0)
				{
					for (CodeValue<ZATown> one : tac.getTowns())
						profile.add(one.getValue());
				}
				
				if (tac.getAreas() != null && tac.getAreas().size() > 0)
				{
					for (CodeValue<ZAArea> one : tac.getAreas())
						profile.add(one.getValue());
				}
				
				if (tac.getCounties() != null && tac.getCounties().size() > 0)
				{
					for (CodeValue<ZACounty> one : tac.getCounties())
						profile.add(one.getValue());
				}
			}
		}
		
		return profile;
	}
}