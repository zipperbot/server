package com.bot.server.service.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZAuth
{
	@JsonProperty("subdomain")
	private String tenant;
	@JsonProperty("access_token")
	private String accessToken;
	@JsonProperty("settings")
	private Settings settings;
	
	public String getTenant()
	{
		return tenant;
	}
	
	public void setTenant(String tenant)
	{
		this.tenant = tenant;
	}
	
	public String getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}
	
	public Settings getSettings()
	{
		return settings;
	}

	public void setSettings(Settings settings)
	{
		this.settings = settings;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Settings
	{
		@JsonProperty("timezone")
		private String timezone;
		@JsonProperty("dateFormat")
		private String dateFormat;
		@JsonProperty("timeFormat")
		private String timeFormat;
		
		public String getTimezone()
		{
			return timezone;
		}
		
		public void setTimezone(String timezone)
		{
			this.timezone = timezone;
		}
		
		public String getDateFormat()
		{
			return dateFormat;
		}
		
		public void setDateFormat(String dateFormat)
		{
			this.dateFormat = dateFormat;
		}
		
		public String getTimeFormat()
		{
			return timeFormat;
		}

		public void setTimeFormat(String timeFormat)
		{
			this.timeFormat = timeFormat;
		}
	}
}