package com.bot.server.service.system;

public enum Target
{
	ZYPRR("Zyprr", "Zipper", "Zy")
	, ZA("ZipperAgent", true, "ZA", "ZypperAgent")
	, GOOGLE("Google")
	;
	
	private String name;
	private String [] synonyms;
	private boolean defaulted;
	
	private Target(String name, boolean defaulted, String... synonyms)
	{
		this.name = name;
		this.defaulted = defaulted;
		this.synonyms = synonyms;
	}
	
	private Target(String name, String... synonyms)
	{
		this(name, false, synonyms);
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String [] getSynonyms()
	{
		return this.synonyms;
	}
	
	public boolean isDefault()
	{
		return defaulted;
	}
	
	public static Target getDefault()
	{
		Target retVal = null;
		
		for (int i=0; i < Target.values().length && retVal == null; i++)
		{
			Target one = Target.values()[i];
			
			if (one.defaulted)
				retVal = one;
		}
		
		return retVal;
	}
}