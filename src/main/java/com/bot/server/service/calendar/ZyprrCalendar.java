package com.bot.server.service.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bot.server.dto.CalendarRequest;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Component
public class ZyprrCalendar implements CalendarI
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired Calendars calendars;
	
	@Value("${ext.zyprr.apiKey}")
	private String apiKey;
	@Value("${ext.zazy.activityAdd}")
	private String activityAdd;
	@Value("${ext.zyprr.token}")
	private String token;
	
	@Override
	public Type getType()
	{
		return Type.ZYPRR;
	}

    @PostConstruct
    public void done()
    {
        if (logger.isDebugEnabled())
            logger.debug("ZyprrCalendar post construct done");
        
        calendars.register(this);
    }
	
	@Override
	public void add(CalendarRequest req) throws Exception
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("subject", req.getSubject());
		data.put("start", req.getStartDate()+" "+req.getStartTime());
		data.put("end", df.format(new Date(df.parse(
				req.getStartDate()+" "+req.getStartTime()).getTime()+(req.getDuration()*60000l))));
		data.put("activityType", "MEETING");
		data.put("activityStatus", "NSTRTD");
		
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(this.activityAdd);
		
		resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey+",access_token="+this.token)
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, data);
	}
	
	public static void main(String [] args) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource("https://demotest.zyprr.com/api/login/process");
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("userName", "tanmoy.bhattacharya@soais.com");
		postData.put("password", "soais123");
		
		resource.type(MediaType.APPLICATION_JSON_TYPE)
			.header("Authorization", "consumer_key=0")
			.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, postData);
	}
}