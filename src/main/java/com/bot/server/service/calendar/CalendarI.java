package com.bot.server.service.calendar;

import com.bot.server.dto.CalendarRequest;

public interface CalendarI
{
	public enum Type
	{
		GOOGLE("google")
		, ZYPRR("zyprr")
		, ZA("za")
		;
		
		private String alias;
		
		private Type(String alias)
		{
			this.alias = alias;
		}
		
		public String alias()
		{
			return this.alias;
		}
	};
	
	public Type getType();
	
	public void add(CalendarRequest req) throws Exception;
}