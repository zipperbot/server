package com.bot.server.service.calendar;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.bot.server.dto.CalendarRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Component
public class GoogleCalendar implements CalendarI
{
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired Calendars calendars;
	
	@Value("${ext.google.appKey}")
	private String appKey;
	@Value("${ext.google.appSecret}")
	private String appSecret;
	@Value("${ext.google.token}")
	private String refreshToken;
	@Value("${ext.google.tokenEndPoint}")
	private String tokenEndPoint;
	
	@Override
	public Type getType()
	{
		return Type.GOOGLE;
	}	
    
    @PostConstruct
    public void done()
    {
        if (logger.isDebugEnabled())
            logger.debug("GoogleCalendar post construct done");
        
        calendars.register(this);
    }
    
	@Override
	public void add(CalendarRequest req) throws Exception
	{
		if (logger.isDebugEnabled())
			logger.debug("GoogleCalendar:add:req = "+req);
		
		Credential cred = new Credential(BearerToken.authorizationHeaderAccessMethod());
		cred.setAccessToken(this.accessToken());
		Calendar cal = new Calendar.Builder(GoogleNetHttpTransport.newTrustedTransport(), 
				JacksonFactory.getDefaultInstance(), cred).build();
		
		Event event = new Event();
		event.setSummary(req.getSubject());
		EventDateTime sd = new EventDateTime();
		Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(req.getStartDate()+" "+req.getStartTime());
		sd.setDateTime(new DateTime(startDate));
		event.setStart(sd);
		EventDateTime ed = new EventDateTime();
		ed.setDateTime(new DateTime(new Date(startDate.getTime()+req.getDuration())));
		event.setEnd(ed);
		cal.events().insert("primary", event).execute();
	}
	
	private String accessToken()
	{
		String retVal = null;
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(this.tokenEndPoint);
		MultivaluedMap<String, String> postData = new MultivaluedMapImpl();
		postData.add("client_id", this.appKey);
		postData.add("client_secret", this.appSecret);
		postData.add("refresh_token", this.refreshToken);
		postData.add("grant_type", "refresh_token");
//		postData.add("access_type", "offline");
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
					.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, postData);
		
        if (HttpStatus.OK == HttpStatus.valueOf(resp.getStatus()))
        {
        	OAuthToken authResp = resp.getEntity(OAuthToken.class);
        	retVal = authResp.accessToken;
        }
        
        return retVal;
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	private static class OAuthToken
	{
		@JsonProperty("access_token")
		private String accessToken;
		@JsonProperty("expires_in")
		private Integer expiresIn;
		@JsonProperty("token_type")
		private String tokenType;
	}
}