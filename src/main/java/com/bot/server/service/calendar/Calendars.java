package com.bot.server.service.calendar;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.bot.server.util.CaseInsensitiveMap;

@Component
public class Calendars
{
	private Map<String, CalendarI> type2Cal = new CaseInsensitiveMap<CalendarI>();
	
	void register(CalendarI calendar)
	{
		type2Cal.put(calendar.getType().alias(), calendar);
	}
	
	public CalendarI get(String alias)
	{
		return type2Cal.get(alias);
	}
}