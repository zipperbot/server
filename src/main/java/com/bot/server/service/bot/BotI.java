package com.bot.server.service.bot;

public interface BotI
{
    public enum Type {GOOGLE};
    
    public Type getType();
    
    public BotResponseI hi();
    
    public BotResponseI query(String q, String sessionId);
}
