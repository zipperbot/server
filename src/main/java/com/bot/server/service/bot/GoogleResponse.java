package com.bot.server.service.bot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bot.server.util.Helper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleResponse implements BotResponseI
{
    private static Map<String, BotResponseI.Type> a2T_ = new HashMap<String, BotResponseI.Type>();
    
    static
    {
        a2T_.put("input.welcome", BotResponseI.Type.CHITCHAT);
        a2T_.put("calendar.meeting", BotResponseI.Type.MEETING);
    }
    
    @JsonProperty("id")
    private String id;
    @JsonProperty("lang")
    private String lang;
    @JsonProperty("sessionId")
    private String sessionId;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("result")
    private Result result;
    
    public String getId()
    {
        return id;
    }

    public String getLang()
    {
        return lang;
    }
    
    public String getSessionId()
    {
        return sessionId;
    }

    public Result getResult()
    {
        return result;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Result
    {
        @JsonProperty("source")
        private String source;
        @JsonProperty("action")
        private String action;
        @JsonProperty("fulfillment")
        private Fulfillment fulfillment;
//        @JsonProperty("parameters")
//        private Params parameters;
        @JsonProperty("parameters")
        private Map<String, Object> params;
        
        public String getSource()
        {
            return source;
        }
        
        public String getAction()
        {
            return action;
        }

        public Fulfillment getFulfillment()
        {
            return fulfillment;
        }

//		public Params getParameters()
//		{
//			return parameters;
//		}
		
		public Map<String, Object> getParameters()
		{
			return params;
		}
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Params
    {
        @JsonProperty("given-name")
        private String name;
        @JsonProperty("date")
        private String startDate;
        @JsonProperty("time")
        private String startTime;
        @JsonProperty("user_target_app")
        private String target;
        @JsonProperty("user_target_object")
        private String object;
        @JsonProperty("user_target_date")
        private String targetDate;
        @JsonProperty("number")
        private Integer number;
        @JsonProperty("any")
        private String any;
        @JsonProperty("startDate")
		private String sDate;
		@JsonProperty("endDate")
		private String eDate;        
		@JsonProperty("town")
		private String town;
		@JsonProperty("maxBudget")
		private Long maxBudget;
		@JsonProperty("minBudget")
		private Long minBudget;
		@JsonProperty("maxBoundary")
		private Duration maxBoundary;
		@JsonProperty("property")
		private String property;
		@JsonProperty("operator")
		private String operator;
		@JsonProperty("geo-city")
		private String city;
		
//        @JsonProperty("Duration")
//        private Duration duration;
        
		
        public String getAny()
        {
        	return any;
        }
        
		public String getsDate()
		{
			return sDate;
		}

		public String geteDate()
		{
			return eDate;
		}

		public String getName()
		{
			return name;
		}
		
		public String getStartDate()
		{
			return startDate;
		}
		
		public String getStartTime()
		{
			return startTime;
		}
		
		public String getTarget()
		{
			return target;
		}
		
		public String getObject()
		{
			return object;
		}
		
		public String getTargetDate()
		{
			return targetDate;
		}
		
		public Integer getNumber()
		{
			return number;
		}

		public String getTown()
		{
			return town;
		}

		public Long getMaxBudget()
		{
			return maxBudget;
		}

		public Long getMinBudget()
		{
			return minBudget;
		}
		
		public Duration getMaxBoundary()
		{
			return maxBoundary;
		}

		public String getProperty()
		{
			return property;
		}

		public String getOperator()
		{
			return operator;
		}

		public String getCity()
		{
			return city;
		}
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Duration
    {
        @JsonProperty("amount")
        private Integer amount;
        @JsonProperty("unit")
        private String unit;
        
		public Integer getAmount()
		{
			return amount;
		}
		
		public String getUnit()
		{
			return unit;
		}
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Fulfillment
    {
        @JsonProperty("speech")
        private String speech;
        @JsonProperty("messages")
        private List<Message> messages;
        
        public String getSpeech()
        {
            return speech;
        }

        public List<Message> getMessages()
        {
            return messages;
        }
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Message
    {
        @JsonProperty("type")
        private int type;
        @JsonProperty("speech")
        private String speech;
        
        public int getType()
        {
            return type;
        }

        public String getSpeech()
        {
            return speech;
        }
    }

    @Override
    public Type getType()
    {
        Type retVal = null;
        
        if (this.getResult() != null && this.getResult().getAction() != null)
            retVal = a2T_.get(this.getResult().getAction());
        
        return retVal;
    }

    @Override
    public String getMessage()
    {
        String retVal = null;
        
        if (this.getResult() != null && this.getResult().getFulfillment() != null 
                && this.getResult().getFulfillment().getMessages() != null 
                && this.getResult().getFulfillment().getMessages().size() > 0)
        {
            retVal = this.getResult().getFulfillment().getMessages().get(0).getSpeech();
        }
        
        return retVal;
    }

    @Override
    public long getTime()
    {
        long retVal = System.currentTimeMillis();
        
        if (!Helper.isNullOrEmpty(this.timestamp))
        {
            try
            {
                retVal = javax.xml.bind.DatatypeConverter.parseDateTime(this.timestamp).getTime().getTime();
            }
            catch (Exception e)
            {
            }
        }
        
        return retVal;
    }
}
