package com.bot.server.service.bot;

public interface BotResponseI
{
    public enum Type {CHITCHAT, EVENT, MEETING};
    
    public Type getType();
    
    public String getMessage();
    
    public long getTime();
}
