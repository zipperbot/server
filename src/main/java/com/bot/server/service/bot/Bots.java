package com.bot.server.service.bot;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Bots
{
    private Map<BotI.Type, BotI> bots = new HashMap<BotI.Type, BotI>();
    private BotI.Type curr = BotI.Type.GOOGLE;
    
    void register(BotI bot)
    {
        bots.put(bot.getType(), bot);
    }
    
    public BotI curr()
    {
        return bots.get(curr);
    }
}