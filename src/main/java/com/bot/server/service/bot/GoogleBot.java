package com.bot.server.service.bot;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bot.server.util.Helper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Component
public class GoogleBot implements BotI
{
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired Bots bots;
    
    @Value("${bot.google.cToken}")
    private String cToken;
    @Value("${bot.google.dToken}")
    private String dToken;
    @Value("${bot.google.endPoint}")
    private String endPoint;
    @Value("${bot.google.apiVersion}")
    private String version;
    
    @Override
    public Type getType()
    {
        return Type.GOOGLE;
    }
    
    @Override
    public GoogleResponse hi()
    {
        String sessionId = UUID.randomUUID().toString();
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        UriBuilder builder = UriBuilder.fromUri(this.endPoint).path("query")
                .queryParam("v", this.version)
                .queryParam("query", "hi")
                .queryParam("lang", "en")
                .queryParam("sessionId", sessionId)
                ;
        
        WebResource webResource = client.resource(builder.build());
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
        		.header("Authorization", "Bearer "+this.cToken).get(ClientResponse.class);
        
        return response.getEntity(GoogleResponse.class);
    }
    
    @Override
    public GoogleResponse query(String q, String sessionId)
    {
    	if (Helper.isNullOrEmpty(sessionId))
    		sessionId = UUID.randomUUID().toString();
    	
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        UriBuilder builder = UriBuilder.fromUri(this.endPoint).path("query")
                .queryParam("v", this.version)
                .queryParam("query", q)
                .queryParam("lang", "en")
                .queryParam("sessionId", sessionId)
                ;
        
        WebResource webResource = client.resource(builder.build());
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).header(
                "Authorization", "Bearer "+this.cToken).get(ClientResponse.class);
        
        return response.getEntity(GoogleResponse.class);
    }
    
    @PostConstruct
    public void done()
    {
        if (logger.isDebugEnabled())
            logger.debug("GoogleBot post construct done");
        
        bots.register(this);
    }
}