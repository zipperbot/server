package com.bot.server.service.crm;

import java.util.HashMap;
import java.util.Map;

public enum ObjectType
{
	CONTACT("contact", "contacts")
	, ACTIVITY("activity", "activities")
	, TASK("task", "tasks")
	, MEETING("meeting", "meetings")
	, CALL("call", "calls")
	, NOTE("note", "notes")
	, ESTATE("estate", "estates", "property", "properties")
	;
	
	private String [] synonyms;
	private static Map<String, ObjectType> s2O_ = new HashMap<String, ObjectType>();
	
	private ObjectType(String... synonyms)
	{
		this.synonyms = synonyms;
	}
	
	static
	{
		for (ObjectType one : ObjectType.values())
		{
			if (one.synonyms != null && one.synonyms.length > 0)
			{
				for (String synonym : one.synonyms)
					s2O_.put(synonym, one);
			}
		}
	}
	
	public static ObjectType get(String name)
	{
		return s2O_.get(name.toLowerCase());
	}
}