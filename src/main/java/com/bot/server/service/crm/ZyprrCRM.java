package com.bot.server.service.crm;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bot.server.service.system.Target;

@Component
public class ZyprrCRM extends ZyZaCRM
{
	@Autowired CRMs crms;
	
	@Value("${ext.zyprr.apiKey}")
	private String apiKey;
	
	public ZyprrCRM()
	{
		super(Target.ZYPRR, 
				ObjectType.ACTIVITY
				, ObjectType.CALL
				, ObjectType.MEETING
				, ObjectType.TASK
				, ObjectType.CONTACT
				, ObjectType.NOTE
		);
	}
	
	@Override
	public Target getType()
	{
		return Target.ZYPRR;
	}
	
    @PostConstruct
    public void done()
    {
        crms.register(this);
    }

	@Override
	protected String apiKey()
	{
		return this.apiKey;
	}
}