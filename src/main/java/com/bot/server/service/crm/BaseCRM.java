package com.bot.server.service.crm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bot.server.service.system.Target;

abstract class BaseCRM implements CRMI
{
	private Target target;
	private List<ObjectType> types;
	
	protected BaseCRM(Target target, ObjectType... types)
	{
		this.target = target;
		
		if (types != null && types.length > 0)
		{
			this.types = new ArrayList<ObjectType>();
			this.types.addAll(Arrays.asList(types));
		}
	}
	
	@Override
	public List<ObjectType> supported()
	{
		return this.types;
	}

	@Override
	public Target getType()
	{
		return this.target;
	}
}