package com.bot.server.service.crm;

import java.util.List;

import com.bot.server.dto.Activity;
import com.bot.server.dto.ActivityOccurrence;
import com.bot.server.dto.Contact;
import com.bot.server.dto.ListResponse;
import com.bot.server.dto.Note;
import com.bot.server.dto.Property;
import com.bot.server.dto.PropertySearchCrit;
import com.bot.server.service.system.Target;

public interface CRMI
{
	public List<ObjectType> supported();
	
	public Target getType();
	
	public Contact add(Contact req) throws Exception;
	
	public Note add(Note req) throws Exception;
	
	public Activity add(Activity req) throws Exception;
	
	public List<Contact> list(String t, int sidx, int count) throws Exception;
	
	public List<Contact> listContact(int sidx, int count, ObjectKind kind) throws Exception;
	
    public List<Contact> similarContact(String name, int count) throws Exception;
	
	public List<Note> listNote(String pid, String ptype, int sidx, int count) throws Exception;
	
	public List<Note> listUnrelatedNote(int sidx, int count) throws Exception;
	
	public List<Activity> listActivity(String pid, String ptype, String type, 
			int sidx, int count) throws Exception;
	
	public List<Activity> listActivity(String type, String l, int sidx, int count) throws Exception;
	
	public List<ActivityOccurrence> listActivity(String type, boolean my, long start, 
			long end, int sidx, int count) throws Exception;
	
	public List<ActivityOccurrence> listActivity(String type, boolean my, String start, 
			String end, int sidx, int count) throws Exception;
	
	public void markActivity(String id, int version, String type, Activity.Status status) throws Exception;
	
	public ListResponse<Property> listProperties(PropertySearchCrit crit, int sidx, int ps) throws Exception;
	
	public ListResponse<Property> listPropertiesWithAddress(String address) throws Exception;
	
	public ListResponse<Property> listProperties(PropertySearchCrit crit, double lat, double lng, 
			double distance, int sidx, int ps) throws Exception;
}