package com.bot.server.service.crm;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.bot.server.service.system.Target;

@Component
public class CRMs
{
	private Map<Target, CRMI> type2Val = new HashMap<Target, CRMI>();
	
	void register(CRMI crm)
	{
		type2Val.put(crm.getType(), crm);
	}
	
	public CRMI get(Target target, ObjectType type) throws Exception
	{
		CRMI retVal = type2Val.get(target);
		
		if (!retVal.supported().contains(type))
			throw new Exception("Object ["+type+"] not supported by ["+target+"]");
		
		return retVal;
	}
}