package com.bot.server.service.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bot.server.dto.ListResponse;
import com.bot.server.dto.Property;
import com.bot.server.dto.PropertySearchCrit;
import com.bot.server.dto.ZAProperty;
import com.bot.server.dto.ZyprrList;
import com.bot.server.dto.ZyprrResponse;
import com.bot.server.service.system.Target;
import com.bot.server.session.Session;
import com.bot.server.util.Helper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Component
public class ZACRM extends ZyZaCRM
{
	@Autowired CRMs crms;
	
	@Value("${ext.za.apiKey}")
	private String apiKey;
	@Value("${ext.za.propertySearch}")
	private String propertySearch;
	@Value("${ext.za.propertyDistance}")
	private String propertyDistance;
	
	private Map<String, String> townMap = new HashMap<String, String>();
	
	public ZACRM()
	{
		super(Target.ZA, 
				ObjectType.ACTIVITY
				, ObjectType.CALL
				, ObjectType.MEETING
				, ObjectType.TASK
				, ObjectType.CONTACT
				, ObjectType.ESTATE
				, ObjectType.NOTE
		);
	}
	
    @PostConstruct
    public void done()
    {
        crms.register(this);
        townMap.put("waltham", "WLTH");
        townMap.put("newton", "NWTN");
        townMap.put("wilmington", "WILM");
        townMap.put("boston", "BOST");
    }

	@Override
	protected String apiKey()
	{
		return this.apiKey;
	}
	
	private String toCrit(PropertySearchCrit crit)
	{
		crit.done();
		StringBuffer advCrit = new StringBuffer();
		
		if (crit.pt() != null)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("apt:").append(crit.pt());
		if (crit.price() > 0l)
		{
			if (crit.op() == null || "LT".equalsIgnoreCase(crit.op()))
				advCrit.append(advCrit.length() > 0 ? ";" : "").append("apmax:").append(crit.price());
			else if ("GT".equalsIgnoreCase(crit.op()))
				advCrit.append(advCrit.length() > 0 ? ";" : "").append("apmin:").append(crit.price());
		}
		if (crit.beds() != 0)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("abeds:").append(crit.beds());
		if (crit.baths() != 0)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("abaths:").append(crit.baths());
		if (!Helper.isNullOrEmpty(crit.town()))
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("atwnnm:").append(crit.town());
		if (crit.sqFt() != 0)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("acarea:").append(crit.sqFt());
		if (crit.status() != null)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("asts:").append(crit.status());
		if (crit.lowDate() != null)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("ud:").append(crit.lowDate());
		if (crit.highDate() != null)
			advCrit.append(advCrit.length() > 0 ? ";" : "").append("udm:").append(crit.highDate());
		
		return advCrit.length() > 0 ? advCrit.toString() : null;
	}
	
	@Override
	public ListResponse<Property> listProperties(PropertySearchCrit crit, int sidx, int ps) throws Exception
	{
		List<Property> retLst = null;
		int count = 0;
		
		if (!crit.isEmpty())
		{
	        ClientConfig cfg = new DefaultClientConfig();
	        cfg.getClasses().add(JacksonJsonProvider.class);
			Client client = Client.create(cfg);
	        client.addFilter(new LoggingFilter());
	        
			WebResource resource = client.resource(targets.get(getType()).getScheme()
					+Session.me().getProfile().getTenant()
					+this.propertySearch+"?crit="+toCrit(crit)+"&sidx="+sidx+"&ps="+ps);
			
			ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
					"Authorization", "consumer_key="+this.apiKey()+",access_token="+
							Session.me().getProfile().getToken())
					.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			
			ZyprrResponse<ZyprrList<ZAProperty>> resp1 = resp.getEntity(
					new GenericType<ZyprrResponse<ZyprrList<ZAProperty>>>(){});
			
			ZyprrList<ZAProperty> objects = resp1.getResult();
			
			if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
			{
				retLst = new ArrayList<Property>();
				
				for (ZAProperty object : objects.getFilteredList())
					retLst.add(object.toSelf());
				
				count = objects.getDataCount();
			}
		}
		
		return ListResponse.neu(Property.class).data(retLst).count(count);
	}
	
	@Override
	public ListResponse<Property> listProperties(PropertySearchCrit crit, double lat, double lng, 
			double distance, int sidx, int ps) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        
        StringBuffer url = new StringBuffer();
        url.append(this.propertyDistance).append("?lat=").append(lat).append("&lng=").append(lng);
        url.append("&distance=").append(distance).append("&sidx=").append(sidx).append("&ps=").append(ps);
        
        if (!crit.isEmpty())
        	url.append("&crit=").append(toCrit(crit));
        
		WebResource resource = client.resource(targets.get(getType()).getScheme()
				+Session.me().getProfile().getTenant()+url);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZAProperty>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZAProperty>>>(){});
		
		ZyprrList<ZAProperty> objects = resp1.getResult();
		List<Property> retLst = null;
		int count = 0;
		
		if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Property>();
			
			for (ZAProperty object : objects.getFilteredList())
				retLst.add(object.toSelf());
			
			count = objects.getDataCount();
		}
		
		return ListResponse.neu(Property.class).data(retLst).count(count);
	}
	
	@Override
	public ListResponse<Property> listPropertiesWithAddress(String address) throws Exception
	{
		List<Property> retLst = null;
		int count = 0;
		
		if (!Helper.isNullOrEmpty(address))
		{
			ClientConfig cfg = new DefaultClientConfig();
	        cfg.getClasses().add(JacksonJsonProvider.class);
			Client client = Client.create(cfg);
	        client.addFilter(new LoggingFilter());
	        
			WebResource resource = client.resource(targets.get(getType()).getScheme()
					+Session.me().getProfile().getTenant()
					+this.propertySearch+"?crit=aflwcaddr:"+address+"&sidx="+0+"&ps="+10);
			
			ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
					"Authorization", "consumer_key="+this.apiKey()+",access_token="+
							Session.me().getProfile().getToken())
					.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			
			ZyprrResponse<ZyprrList<ZAProperty>> resp1 = resp.getEntity(
					new GenericType<ZyprrResponse<ZyprrList<ZAProperty>>>(){});
			
			ZyprrList<ZAProperty> objects = resp1.getResult();
			
			if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
			{
				retLst = new ArrayList<Property>();
				
				for (ZAProperty object : objects.getFilteredList())
					retLst.add(object.toSelf());
				
				count = objects.getDataCount();
			}
		}
		
		return ListResponse.neu(Property.class).data(retLst).count(count);
	}
}