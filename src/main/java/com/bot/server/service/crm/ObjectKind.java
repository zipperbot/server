package com.bot.server.service.crm;

import java.util.HashMap;
import java.util.Map;

public enum ObjectKind
{
	BEST("best")
	, MOST("most")
	, LEAST("least")
	;
	
	private String alias;
	private static Map<String, ObjectKind> map_ = new HashMap<String, ObjectKind>();
	
	private ObjectKind(String alias)
	{
		this.alias = alias;
	}
	
	static
	{
		for (ObjectKind kind : ObjectKind.values())
			map_.put(kind.alias, kind);
	}
	
	public static ObjectKind get(String alias)
	{
		return map_.get(alias);
	}
}