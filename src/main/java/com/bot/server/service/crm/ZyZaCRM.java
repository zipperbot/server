package com.bot.server.service.crm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bot.server.dto.Activity;
import com.bot.server.dto.ActivityOccurrence;
import com.bot.server.dto.Contact;
import com.bot.server.dto.ListResponse;
import com.bot.server.dto.Note;
import com.bot.server.dto.Property;
import com.bot.server.dto.PropertySearchCrit;
import com.bot.server.dto.ZyprrActivity;
import com.bot.server.dto.ZyprrActivityOccurrence;
import com.bot.server.dto.ZyprrBulkRequest;
import com.bot.server.dto.ZyprrContact;
import com.bot.server.dto.ZyprrList;
import com.bot.server.dto.ZyprrNote;
import com.bot.server.dto.ZyprrResponse;
import com.bot.server.service.system.Target;
import com.bot.server.service.system.Targets;
import com.bot.server.session.Session;
import com.bot.server.util.Helper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

abstract class ZyZaCRM extends BaseCRM
{
	@Value("${ext.zazy.contactAdd}")
	protected String contactAdd;
	@Value("${ext.zazy.contactList}")
	protected String contactList;
    @Value("${ext.zazy.contactSimilar}")
    protected String contactSimilar;
	
	@Value("${ext.zazy.noteAdd}")
	protected String noteAdd;
	@Value("${ext.zazy.noteUpdate}")
	protected String noteUpdate;
	@Value("${ext.zazy.noteList}")
	protected String noteList;
	@Value("${ext.zazy.noteUnrelatedList}")
	protected String noteUnrelatedList;
	
	@Value("${ext.zazy.activityAdd}")
	protected String activityAdd;
	@Value("${ext.zazy.activityList}")
	protected String activityList;
	@Value("${ext.zazy.activityEvent}")
	protected String activityEvent;
	@Value("${ext.zazy.activityBulk}")
	protected String activityBulk;
	
	@Autowired protected Targets targets;
	
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	protected ZyZaCRM(Target target, ObjectType... types)
	{
		super(target, types);
	}
	
	protected abstract String apiKey();
	
	@Override
	public Contact add(Contact req) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()+this.contactAdd);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, req.toZyprr());
		
		ZyprrResponse<ZyprrContact> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrContact>>(){});
		
		return resp1 != null && resp1.getResult() != null 
				? resp1.getResult().toSelf() : null;
	}
	
	@Override
    public List<Contact> similarContact(String name, int count) throws Exception
    {
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        //?l=all&o=uo:DESC&sidx=0&ps=<ps>
        UriBuilder uri = UriBuilder.fromPath(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()+this.contactSimilar)
            .queryParam("name", name)
            .queryParam("sidx", "0")
            .queryParam("ps", count);
        
        WebResource resource = client.resource(uri.build());
        
        ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
                "Authorization", "consumer_key="+this.apiKey()+",access_token="+Session.me().getProfile().getToken())
                .accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
        
        ZyprrResponse<List<ZyprrContact>> resp1 = resp.getEntity(
                new GenericType<ZyprrResponse<List<ZyprrContact>>>(){});
        
        List<ZyprrContact> contacts = resp1.getResult();
        List<Contact> retLst = null;
        
        if (contacts != null && contacts.size() > 0)
        {
            retLst = new ArrayList<Contact>();
            
            for (ZyprrContact contact : contacts)
                retLst.add(contact.toSelf());
        }
        
        return retLst;
    }

	@Override
	public List<Contact> listContact(int sidx, int count, ObjectKind kind) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        //?l=all&o=uo:DESC&sidx=0&ps=<ps>
        UriBuilder uri = UriBuilder.fromPath(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()+this.contactList)
			.queryParam("l", "all")
			.queryParam("sidx", "0")
			.queryParam("ps", count);
        
        if (kind == ObjectKind.MOST)
			uri = uri.queryParam("o", "uo:DESC");
        else if (kind == ObjectKind.LEAST)
			uri = uri.queryParam("o", "uo:ASC");
        
		WebResource resource = client.resource(uri.build());
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrContact>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrContact>>>(){});
		
		ZyprrList<ZyprrContact> contacts = resp1.getResult();
		List<Contact> retLst = null;
		
		if (contacts != null && contacts.getFilteredList() != null && contacts.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Contact>();
			
			for (ZyprrContact contact : contacts.getFilteredList())
				retLst.add(contact.toSelf());
		}
		
		return retLst;
	}
	
	@Override
	public List<Contact> list(String t, int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        //?l=all&o=uo:DESC&sidx=0&ps=<ps>
        UriBuilder uri = UriBuilder.fromPath(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()+this.contactList)
			.queryParam("l", "all")
			.queryParam("o", "uo:DESC")
			.queryParam("sidx", "0")
			.queryParam("ps", count);
        
        if (!Helper.isNullOrEmpty(t))
        	uri = uri.queryParam("t", t);
        
		WebResource resource = client.resource(uri.build());
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrContact>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrContact>>>(){});
		
		ZyprrList<ZyprrContact> contacts = resp1.getResult();
		List<Contact> retLst = null;
		
		if (contacts != null && contacts.getFilteredList() != null && contacts.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Contact>();
			
			for (ZyprrContact contact : contacts.getFilteredList())
				retLst.add(contact.toSelf());
		}
		
		return retLst;
	}

	@Override
	public Note add(Note req) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
		        +(req.getId() == null ? this.noteAdd : this.noteUpdate));
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, req.toZyprr());
		
		ZyprrResponse<ZyprrNote> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrNote>>(){});
		
		return resp1 != null && resp1.getResult() != null 
				? resp1.getResult().toSelf() : null;
	}
	
	@Override
	public Activity add(Activity req) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()+this.activityAdd);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, req.toZyprr());
		
		ZyprrResponse<ZyprrActivity> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrActivity>>(){});
		
		return resp1 != null && resp1.getResult() != null 
				? resp1.getResult().toSelf() : null;
	}
	
	@Override
	public List<Note> listUnrelatedNote(int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        String url = targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
				+this.noteUnrelatedList+"?sidx="+sidx+"&ps="+count+"&o=DESC";
        
        if (logger.isDebugEnabled())
        	logger.debug("hitting zyzacrm:listUnrelatedNote ["+sidx+", "+count+"] url = "+url);
        
		WebResource resource = client.resource(url);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrNote>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrNote>>>(){});
		
		ZyprrList<ZyprrNote> objects = resp1.getResult();
		List<Note> retLst = null;
		
		if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Note>();
			
			for (ZyprrNote object : objects.getFilteredList())
				retLst.add(object.toSelf());
		}
		
		return retLst;
	}
	
	@Override
	public List<Note> listNote(String pid, String ptype, int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        StringBuffer sb = new StringBuffer();
        sb.append(this.noteList).append("?l=all").append("&sidx=").append(sidx).append("&ps=").append(count);
        
        if (pid != null && ptype != null)
        	sb.append("&pid=").append(pid).append("&ptyp=").append(ObjectType.get(ptype).name());
        
		WebResource resource = client.resource(targets.get(getType()).getScheme()
				+Session.me().getProfile().getTenant()+sb.toString());
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrNote>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrNote>>>(){});
		
		ZyprrList<ZyprrNote> objects = resp1.getResult();
		List<Note> retLst = null;
		
		if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Note>();
			
			for (ZyprrNote object : objects.getFilteredList())
				retLst.add(object.toSelf());
		}
		
		return retLst;
	}
	
	@Override
	public List<Activity> listActivity(String type, String l, int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
        String f = "";
        
        ObjectType objTyp = ObjectType.get(type);
        
        if (objTyp != null || ObjectType.ACTIVITY != objTyp)
        	f = "&f=typ:"+ObjectType.get(type).name();
        
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
				+this.activityList+"?l="+l+"&sidx="+sidx+"&ps="+count+f);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrActivity>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrActivity>>>(){});
		
		ZyprrList<ZyprrActivity> objects = resp1.getResult();
		List<Activity> retLst = null;
		
		if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Activity>();
			
			for (ZyprrActivity object : objects.getFilteredList())
				retLst.add(object.toSelf());
		}
		
		return retLst;
	}

	@Override
	public List<ActivityOccurrence> listActivity(String type, boolean my, long start,
			long end, int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
				+this.activityEvent+"?myEvent="+my+"&start="+start+"&end="+end+"&activityType="+ObjectType.get(type).name()
				+"&sidx="+sidx+"&ps="+count);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<List<ZyprrActivityOccurrence>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<List<ZyprrActivityOccurrence>>>(){});
		
		List<ZyprrActivityOccurrence> objects = resp1.getResult();
		List<ActivityOccurrence> retLst = null;
		
		if (objects != null && objects.size() > 0)
		{
			retLst = new ArrayList<ActivityOccurrence>();
			
			for (ZyprrActivityOccurrence object : objects)
				retLst.add(object.toSelf());
		}
		
		return retLst;
	}
	
	@Override
	public List<ActivityOccurrence> listActivity(String type, boolean my, String start, 
			String end, int sidx, int count) throws Exception
	{
		long st = 0l;
		long et = 0l;
		start = start.trim();
		end = end.trim();
		
		if (start.indexOf(" ") > -1)
			st = DATE_TIME_FORMAT.parse(start).getTime();
		else
			st = DATE_FORMAT.parse(start).getTime();
		
		if (end.indexOf(" ") > -1)
			et = DATE_TIME_FORMAT.parse(end).getTime();
		else
			et = DATE_FORMAT.parse(end).getTime();
		
		return this.listActivity(type, my, st, et, sidx, count);
	}
	
	@Override
	public List<Activity> listActivity(String pid, String ptype, String type, 
			int sidx, int count) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
				+this.activityList+"?l=all&pid="+pid+"&ptyp="+ObjectType.get(ptype).name()
				+"&sidx="+sidx+"&ps="+count+"&f=typ:"+ObjectType.get(type).name());
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="+
						Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		
		ZyprrResponse<ZyprrList<ZyprrActivity>> resp1 = resp.getEntity(
				new GenericType<ZyprrResponse<ZyprrList<ZyprrActivity>>>(){});
		
		ZyprrList<ZyprrActivity> objects = resp1.getResult();
		List<Activity> retLst = null;
		
		if (objects != null && objects.getFilteredList() != null && objects.getFilteredList().size() > 0)
		{
			retLst = new ArrayList<Activity>();
			
			for (ZyprrActivity object : objects.getFilteredList())
				retLst.add(object.toSelf());
		}
		
		return retLst;
	}
	
	@Override
	public void markActivity(String id, int version, String type, Activity.Status status) throws Exception
	{
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cfg);
        client.addFilter(new LoggingFilter());
		WebResource resource = client.resource(targets.get(getType()).getScheme()+Session.me().getProfile().getTenant()
				+this.activityBulk);
		
		ClientResponse resp = resource.type(MediaType.APPLICATION_JSON_TYPE).header(
				"Authorization", "consumer_key="+this.apiKey()+",access_token="
						+Session.me().getProfile().getToken())
				.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, 
					ZyprrBulkRequest.newInstance().addField("activityStatus", 
							ZyprrActivity.Status.toStatus(status).name()).addId(id, version));
		
		ZyprrResponse<Object> resp1 = resp.getEntity(new GenericType<ZyprrResponse<Object>>(){});
		
		if (!ZyprrResponse.SUCCESS.equals(resp1.getStatus()))
			throw new Exception("Can't update");
	}
	
	@Override
	public ListResponse<Property> listProperties(PropertySearchCrit crit, int sidx, int ps) throws Exception
	{
		throw new Exception("implement me");
	}
	
	@Override
	public ListResponse<Property> listProperties(PropertySearchCrit crit, double lat, double lng, 
			double distance, int sidx, int ps) throws Exception
	{
		throw new Exception("implement me");
	}
	
	@Override
	public ListResponse<Property> listPropertiesWithAddress(String address) throws Exception
	{
		throw new Exception("implement me");
	}
}