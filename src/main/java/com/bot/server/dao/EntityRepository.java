package com.bot.server.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bot.server.model.dto.DTO;
import com.bot.server.model.entity.Persistent;

public interface EntityRepository<E extends Persistent<E, D>, D extends DTO<D, E>> extends JpaRepository<E, String>
{

}