package com.bot.server.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EntityScan(basePackages = {"com.bot.server.model.entity", "com.bot.server.model.entity.hr"})
@EnableJpaRepositories("com.bot.server.dao")
@EnableTransactionManagement
public class RepositoryConfig
{

}