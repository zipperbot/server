package com.bot.server.util;

import java.util.HashMap;

public class CaseInsensitiveMap<V> extends HashMap<String, V>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public V put(String key, V value)
	{
		return super.put(key.toLowerCase(), value);
	}
	
	@Override
	public V get(Object key)
	{
		return super.get(((String)key).toLowerCase());
	}
}