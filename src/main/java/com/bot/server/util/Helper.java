package com.bot.server.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper
{
	private static final String ISO8601 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	private static final String ISO8601_DO = "yyyy-MM-dd";
	private static final DateFormat ISO8601_DF = new SimpleDateFormat(ISO8601);
	private static final DateFormat ISO8601_DO_DF = new SimpleDateFormat(ISO8601_DO);
	
    public static boolean isNullOrEmpty(String arg)
    {
        return arg == null || arg.trim().length() < 1;
    }
    
    public static Date parse(String arg) throws Exception
    {
    	return ISO8601_DF.parse(arg);
    }
    
    public static Date parseDate(String arg) throws Exception
    {
    	return ISO8601_DO_DF.parse(arg);
    }
    
    public static String format(Date arg) throws Exception
    {
    	return ISO8601_DF.format(arg);
    }
    
    public static String formatDate(Date arg) throws Exception
    {
    	return ISO8601_DO_DF.format(arg);
    }
}