package com.bot.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Log
{
	DEBUG(new LoggerI()
	{
		@Override
		public boolean isEnabled()
		{
			return logger_.isDebugEnabled();
		}

		@Override
		public void log(String message)
		{
			logger_.debug(message);
		}
	})
	, INFO(new LoggerI()
	{
		@Override
		public boolean isEnabled()
		{
			return logger_.isInfoEnabled();
		}

		@Override
		public void log(String message)
		{
			logger_.info(message);
		}
	})
	, WARN(new LoggerI()
	{
		@Override
		public boolean isEnabled()
		{
			return logger_.isWarnEnabled();
		}

		@Override
		public void log(String message)
		{
			logger_.warn(message);
		}
	})
	, ERROR(new LoggerI()
	{
		@Override
		public boolean isEnabled()
		{
			return logger_.isErrorEnabled();
		}

		@Override
		public void log(String message)
		{
			logger_.error(message);
		}
	})
	;
	
	private static Logger logger_ = LoggerFactory.getLogger(Log.class);
	private LoggerI loggerI;
	
	private Log(LoggerI arg)
	{
		this.loggerI = arg;
	}
	
	public boolean isEnabled()
	{
		return this.loggerI.isEnabled();
	}
	
	public void log(String message)
	{
		this.loggerI.log(message);
	}
	
	private static interface LoggerI
	{
		public boolean isEnabled();
		
		public void log(String message);
	}
}