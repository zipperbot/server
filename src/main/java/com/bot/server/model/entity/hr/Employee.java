package com.bot.server.model.entity.hr;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.hr.EmployeeDTO;
import com.bot.server.model.entity.Persistent;

@Entity
@Table(name="EMPLOYEE")
public class Employee extends Persistent<Employee, EmployeeDTO>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne(optional=false)
    @JoinColumn(name = "USER_ID", unique=true, nullable=false, updatable=false) 
    private User user;
	
	@ManyToOne(optional=true)
    @JoinColumn(name = "SUPERVISOR_ID") 
    private Employee supervisor;
	
	@OneToMany(mappedBy="supervisor",targetEntity=Employee.class, 
			fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private Set<Employee> reportees;
	
	@OneToMany(mappedBy="from",targetEntity=EmployeeFeedback.class, 
			fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private Set<EmployeeFeedback> produced;
	
	@OneToMany(mappedBy="to",targetEntity=EmployeeFeedback.class, 
			fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private Set<EmployeeFeedback> consumed;	

	@Column(name="JOB_CD")
	private String jobCd;
	
	@Column(name="JOB_TITLE")
	private String jobTitle;
	
	@Column(name="ORG_PATH", length=2000)
	private String orgPath;
	
	@Column(name="HIRED_ON", updatable=false)
	private Date hiredOn;
	
	public Employee()
	{
		super(EntityType.EMPLOYEE);
	}
	
	public Employee(EmployeeDTO that)
	{
		super(that, EntityType.EMPLOYEE);
		this.user = that.getUser().toEntity();
		this.supervisor = that.getSupervisor() != null ? that.getSupervisor().toEntity() : null;
		this.jobCd = that.getJobCd();
		this.jobTitle = that.getJobTitle();
		this.hiredOn = that.getHiredOn();
		this.orgPath = that.getOrgPath();
	}
	
	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public Employee getSupervisor()
	{
		return supervisor;
	}

	public void setSupervisor(Employee supervisor)
	{
		this.supervisor = supervisor;
	}

	public Set<Employee> getReportees()
	{
		return reportees;
	}

	public void setReportees(Set<Employee> reportees)
	{
		this.reportees = reportees;
	}

	public String getJobCd()
	{
		return jobCd;
	}

	public void setJobCd(String jobCd)
	{
		this.jobCd = jobCd;
	}

	public String getJobTitle()
	{
		return jobTitle;
	}

	public void setJobTitle(String jobTitle)
	{
		this.jobTitle = jobTitle;
	}
	
	public String getOrgPath()
	{
		return orgPath;
	}

	public void setOrgPath(String orgPath)
	{
		this.orgPath = orgPath;
	}

	public Date getHiredOn()
	{
		return hiredOn;
	}

	public void setHiredOn(Date hiredOn)
	{
		this.hiredOn = hiredOn;
	}
	
	public Set<EmployeeFeedback> getProduced()
	{
		return produced;
	}

	public void setProduced(Set<EmployeeFeedback> produced)
	{
		this.produced = produced;
	}

	public Set<EmployeeFeedback> getConsumed()
	{
		return consumed;
	}

	public void setConsumed(Set<EmployeeFeedback> consumed)
	{
		this.consumed = consumed;
	}

	@Override
	protected EmployeeDTO newDTO(Hydration type)
	{
		return new EmployeeDTO(this, type);
	}
}