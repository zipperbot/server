package com.bot.server.model.entity.hr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.hr.EmployeeFeedbackDTO;
import com.bot.server.model.entity.Persistent;

@Entity
@Table(name="EMPLOYEE_FEEDBACK")
public class EmployeeFeedback extends Persistent<EmployeeFeedback, EmployeeFeedbackDTO>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(optional=false)
    @JoinColumn(name = "FROM_EMP") 
    private Employee from;
	
	@ManyToOne(optional=false)
    @JoinColumn(name = "TO_EMP") 
    private Employee to;
	
	@Column(name="CONTEXT")
	private String context;
	
	@Lob
	@Column(name="FEEDBACK")
	private String feedback;
	
	@Column(name="SENTIMENT_SCORE")
	private Integer score;
	
	@Column(name="RESTRICTED", nullable=false)
	private boolean restricted;
	
	public EmployeeFeedback()
	{
		super(EntityType.FEEDBACK);
	}
	
	public EmployeeFeedback(EmployeeFeedbackDTO that)
	{
		super(that, EntityType.FEEDBACK);
		this.from = that.getFrom().toEntity();
		this.to = that.getTo().toEntity();
		this.context = that.getContext();
		this.feedback = that.getFeedback();
		this.score = that.getScore();
		this.restricted = that.isRestricted();
	}

	public Employee getFrom()
	{
		return from;
	}

	public void setFrom(Employee from)
	{
		this.from = from;
	}

	public Employee getTo()
	{
		return to;
	}

	public void setTo(Employee to)
	{
		this.to = to;
	}

	public String getContext()
	{
		return context;
	}

	public void setContext(String context)
	{
		this.context = context;
	}

	public String getFeedback()
	{
		return feedback;
	}

	public void setFeedback(String feedback)
	{
		this.feedback = feedback;
	}

	public Integer getScore()
	{
		return score;
	}

	public void setScore(Integer score)
	{
		this.score = score;
	}

	public boolean isRestricted()
	{
		return restricted;
	}

	public void setRestricted(boolean restricted)
	{
		this.restricted = restricted;
	}

	@Override
	protected EmployeeFeedbackDTO newDTO(Hydration type)
	{
		return new EmployeeFeedbackDTO(this, type);
	}
}