package com.bot.server.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.DTO;
import com.bot.server.session.Session;

@MappedSuperclass
public abstract class Persistent<E extends Persistent<E, D>, D extends DTO<D, E>> implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	
	@Version
	@Column(name="VERSION", nullable=false)
	private int version;
	
	@Column(name="CREATED_ON", nullable=false)
	private Date createdOn;
	
	@Column(name="CREATED_BY", nullable=false)
	private String createdBy;
	
	@Column(name="UPDATED_ON")
	private Date updatedOn;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	private transient EntityType<E, D> template;
	
	protected Persistent(EntityType<E, D> template)
	{
		this.template = template;
	}
	
	protected Persistent(D that, EntityType<E, D> template)
	{
		this.template = template;
		this.id = that.getId();
		this.version = that.getVersion();
	}
	
	public D toDTO()
	{
		return toDTO(Hydration.SELF);
	}
	
	public D toDTO(Hydration type)
	{
		return newDTO(type);
	}
	
	protected abstract D newDTO(Hydration type);
	
	public EntityType<E, D> template()
	{
		return this.template;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}
	
	public Date getCreatedOn()
	{
		return createdOn;
	}

	public void setCreatedOn(Date createdOn)
	{
		this.createdOn = createdOn;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn()
	{
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn)
	{
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	@PrePersist
	public void onPreInsert()
	{
		this.createdOn = new Date();
		this.createdBy = Session.ensuredMe().getProfile().getHandle();
	}
	
	@PreUpdate
	public void onPreUpdate()
	{
		this.updatedOn = new Date();
		this.updatedBy = Session.ensuredMe().getProfile().getHandle();
	}
}