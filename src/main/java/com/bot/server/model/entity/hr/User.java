package com.bot.server.model.entity.hr;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.hr.UserDTO;
import com.bot.server.model.entity.Persistent;

@Entity
@Table(name="USER")
public class User extends Persistent<User, UserDTO>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Status {EMPLOYEE, CONSULTANT, CONTRACTOR};
	public static enum Role {NO_ACCESS, ADMIN, MGR, USER};
	
	public static enum TimeZone
	{
		IST("Asia/Kolkata", "Indian Standard Time")
		, ET("America/New_York", "Eastern Time")
		, PT("America/Los_Angeles", "Pacific Time")
		, CT("America/Chicago", "Central Time")
		, MT("America/Denver", "Mountain Time")
		;
		
		private String actual;
		private String desc;
		
		private TimeZone(String actual, String desc)
		{
			this.actual = actual;
			this.desc = desc;
		}
		
		public String actual()
		{
			return this.actual;
		}
		
		public String desc()
		{
			return this.desc;
		}
	}
	
	@Column(name="TEAM_ID", nullable=false)
	private String teamId;

	@Column(name="USER_ID", nullable=false)
	private String userId;
	
	@Column(name="STATUS", nullable=false)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(name="FIRST_NM")
	private String firstName;
	
	@Column(name="MID_NM")
	private String middleName;
	
	@Column(name="LAST_NM", nullable=false)
	private String lastName;
	
	@Column(name="EMAIL", nullable=false, unique=true)
	private String email;
	
	@Column(name="PHONE")
	private String phone;
	
	@Column(name="ROLE", nullable=false)
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@Column(name="TIME_ZONE", nullable=false)
	@Enumerated(EnumType.STRING)
	private TimeZone timeZone;
	
	@Column(name="EMAIL_DISABLED", nullable=false)
	private boolean emailDisabled;
	
	@Column(name="MOBILE_DISABLED", nullable=false)
	private boolean mobileDisabled;
	
	@Column(name="SMS_DISABLED", nullable=false)
	private boolean smsDisabled;
	
	@OneToOne(optional=true, cascade=CascadeType.REMOVE, mappedBy="user", targetEntity=Employee.class)	
	private Employee employment;
	
	public User()
	{
		super(EntityType.USER);
	}
	
	public User(UserDTO that)
	{
		super(that, EntityType.USER);
	}
	
	@Override
	protected UserDTO newDTO(Hydration type)
	{
		return new UserDTO(this, type);
	}

	public String getTeamId()
	{
		return teamId;
	}

	public void setTeamId(String teamId)
	{
		this.teamId = teamId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public Employee getEmployment()
	{
		return employment;
	}

	public void setEmployment(Employee employment)
	{
		this.employment = employment;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Role getRole()
	{
		return role;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}

	public TimeZone getTimeZone()
	{
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone)
	{
		this.timeZone = timeZone;
	}

	public boolean isEmailDisabled()
	{
		return emailDisabled;
	}

	public void setEmailDisabled(boolean emailDisabled)
	{
		this.emailDisabled = emailDisabled;
	}

	public boolean isMobileDisabled()
	{
		return mobileDisabled;
	}

	public void setMobileDisabled(boolean mobileDisabled)
	{
		this.mobileDisabled = mobileDisabled;
	}

	public boolean isSmsDisabled()
	{
		return smsDisabled;
	}

	public void setSmsDisabled(boolean smsDisabled)
	{
		this.smsDisabled = smsDisabled;
	}
}