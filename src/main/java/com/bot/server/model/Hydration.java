package com.bot.server.model;

public enum Hydration
{
	ID, SELF, LIST, SEARCH, ALL;
	
	private int rank;
	
	static
	{
		for (int i=0; i < Hydration.values().length; i++)
			Hydration.values()[i].rank = i;
	}
	
	public boolean ensure(Hydration that)
	{
		return this.rank <= that.rank;
	}
}