package com.bot.server.model;

import com.bot.server.model.dto.DTO;
import com.bot.server.model.dto.hr.EmployeeDTO;
import com.bot.server.model.dto.hr.EmployeeFeedbackDTO;
import com.bot.server.model.dto.hr.UserDTO;
import com.bot.server.model.entity.Persistent;
import com.bot.server.model.entity.hr.Employee;
import com.bot.server.model.entity.hr.EmployeeFeedback;
import com.bot.server.model.entity.hr.User;

public class EntityType<E extends Persistent<E, D>, D extends DTO<D, E>>
{
	private Class<E> entityType;
	private Class<D> dtoType;
	
	public static EntityType<User, UserDTO> USER = new EntityType<User, UserDTO>(User.class, UserDTO.class);
	public static EntityType<Employee, EmployeeDTO> EMPLOYEE = new EntityType<Employee, EmployeeDTO>(Employee.class, EmployeeDTO.class);
	public static EntityType<EmployeeFeedback, EmployeeFeedbackDTO> FEEDBACK = new EntityType<EmployeeFeedback, EmployeeFeedbackDTO>(EmployeeFeedback.class, EmployeeFeedbackDTO.class);
	
	private EntityType(Class<E> entityType, Class<D> dtoType)
	{
		this.entityType = entityType;
		this.dtoType = dtoType;
	}
	
	public Class<E> entityType()
	{
		return entityType;
	}
	
	public Class<D> dtoType()
	{
		return dtoType;
	}
}