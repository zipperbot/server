package com.bot.server.model.dto;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.entity.Persistent;

public abstract class DTO<D extends DTO<D, E>, E extends Persistent<E, D>>
{
	private String id;
	private int version;
	private EntityType<E, D> template;
	
	protected DTO(EntityType<E, D> template)
	{
		this.template = template;
	}
	
	protected DTO(E entity, Hydration type, EntityType<E, D> template)
	{
		this.template = template;
		this.id = entity.getId();
		
		if (type.ensure(Hydration.SELF))
			this.version = entity.getVersion();
	}
	
	public E toEntity()
	{
		return newEntity();
	}
	
	protected abstract E newEntity();
	
	public EntityType<E, D> template()
	{
		return this.template;
	}
	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return version;
	}

	public void setVersion(int version)
	{
		this.version = version;
	}
}