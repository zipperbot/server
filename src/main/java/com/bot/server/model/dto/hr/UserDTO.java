package com.bot.server.model.dto.hr;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.DTO;
import com.bot.server.model.entity.hr.User;

public class UserDTO extends DTO<UserDTO, User>
{
	public UserDTO()
	{
		super(EntityType.USER);
	}
	
	public UserDTO(User that, Hydration type)
	{
		super(that, type, EntityType.USER);
	}
	
	@Override
	protected User newEntity()
	{
		// TODO Auto-generated method stub
		return null;
	}
}