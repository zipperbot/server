package com.bot.server.model.dto.hr;

import java.util.Date;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.DTO;
import com.bot.server.model.entity.hr.Employee;

public class EmployeeDTO extends DTO<EmployeeDTO, Employee>
{
    private UserDTO user;
    private EmployeeDTO supervisor;
	private String jobCd;
	private String jobTitle;
	private String orgPath;
	private Date hiredOn;
	
	public EmployeeDTO()
	{
		super(EntityType.EMPLOYEE);
	}
	
	public EmployeeDTO(Employee that, Hydration type)
	{
		super(that, type, EntityType.EMPLOYEE);
		
		if (type.ensure(Hydration.SELF))
		{
			this.user = that.getUser() != null ? that.getUser().toDTO() : null;
			this.supervisor = that.getSupervisor() != null ? that.getSupervisor().toDTO() : null;
			this.jobCd = that.getJobCd();
			this.jobTitle = that.getJobTitle();
			this.hiredOn = that.getHiredOn();
			this.orgPath = that.getOrgPath();
		}
	}	
	
	public UserDTO getUser()
	{
		return user;
	}

	public void setUser(UserDTO user)
	{
		this.user = user;
	}

	public EmployeeDTO getSupervisor()
	{
		return supervisor;
	}

	public void setSupervisor(EmployeeDTO supervisor)
	{
		this.supervisor = supervisor;
	}

	public String getJobCd()
	{
		return jobCd;
	}

	public void setJobCd(String jobCd)
	{
		this.jobCd = jobCd;
	}

	public String getJobTitle()
	{
		return jobTitle;
	}

	public void setJobTitle(String jobTitle)
	{
		this.jobTitle = jobTitle;
	}
	
	public String getOrgPath()
	{
		return orgPath;
	}

	public void setOrgPath(String orgPath)
	{
		this.orgPath = orgPath;
	}

	public Date getHiredOn()
	{
		return hiredOn;
	}

	public void setHiredOn(Date hiredOn)
	{
		this.hiredOn = hiredOn;
	}

	@Override
	protected Employee newEntity()
	{
		return new Employee(this);
	}
}