package com.bot.server.model.dto.hr;

import com.bot.server.model.EntityType;
import com.bot.server.model.Hydration;
import com.bot.server.model.dto.DTO;
import com.bot.server.model.entity.hr.EmployeeFeedback;

public class EmployeeFeedbackDTO extends DTO<EmployeeFeedbackDTO, EmployeeFeedback>
{
    private EmployeeDTO from;
    private EmployeeDTO to;
	private String context;
	private String feedback;
	private Integer score;
	private boolean restricted;

	public EmployeeFeedbackDTO()
	{
		super(EntityType.FEEDBACK);
	}
	
	public EmployeeFeedbackDTO(EmployeeFeedback that, Hydration type)
	{
		super(that, type, EntityType.FEEDBACK);
		
		if (type.ensure(Hydration.SELF))
		{
			this.from = that.getFrom().toDTO();
			this.to = that.getTo().toDTO();
			this.context = that.getContext();
			this.feedback = that.getFeedback();
			this.score = that.getScore();
			this.restricted = that.isRestricted();
		}
	}

	public EmployeeDTO getFrom()
	{
		return from;
	}

	public void setFrom(EmployeeDTO from)
	{
		this.from = from;
	}

	public EmployeeDTO getTo()
	{
		return to;
	}

	public void setTo(EmployeeDTO to)
	{
		this.to = to;
	}

	public String getContext()
	{
		return context;
	}

	public void setContext(String context)
	{
		this.context = context;
	}

	public String getFeedback()
	{
		return feedback;
	}

	public void setFeedback(String feedback)
	{
		this.feedback = feedback;
	}

	public Integer getScore()
	{
		return score;
	}

	public void setScore(Integer score)
	{
		this.score = score;
	}

	public boolean isRestricted()
	{
		return restricted;
	}

	public void setRestricted(boolean restricted)
	{
		this.restricted = restricted;
	}

	@Override
	protected EmployeeFeedback newEntity()
	{
		return new EmployeeFeedback(this);
	}
}