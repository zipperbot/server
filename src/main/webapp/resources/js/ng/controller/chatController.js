(function(angular, $, undefined) {
	$(document).ready(function() {
		angular.bootstrap(document.getElementById("chatDiv"),['chatApp']);
	});

	var chatApp = angular.module("chatApp",['ngRoute','ui.bootstrap','ngSanitize']);
	
	chatApp.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
		$routeProvider.when('/home.html', {
		  	 title: 'Chat',
		  	 templateUrl: '/resources/html/chat.html',
		     reloadOnSearch:false
		}).when('/', {
		  	 title: 'Chat',
		  	 templateUrl: '/resources/html/chat.html',
		     reloadOnSearch:false
		}); 
		
		$locationProvider.html5Mode(true);
     
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};    
		}    
		
		$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 01 Jan 1900 05:00:00 GMT';
		$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
		$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
	}]);

	chatApp.run(['$rootScope','$location', '$routeParams','$http', function($rootScope, $location, $routeParams, $http){
		$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
			if($location.path() == '/home.html'){	
		 		$rootScope.$broadcast('chat');
		 	}
			else if($location.path() == '/'){	
		 		$rootScope.$broadcast('chat');
		 	}
		});
		$rootScope.$on('$routeUpdate', function(current, pre) {
			$rootScope.$broadcast('chat');
		});
	}]);

	chatApp.controller("chatController",['$scope','$http','$location','$window','$routeParams','$timeout','$rootScope', function($scope, $http, $location, $window, $routeParams, $timeout, $rootScope) {
		$scope.messages = [];
		$scope.sessionId = null;
		$scope.targets = [];
		$scope.targets.push({'code':'google', 'value':'Google'});
		$scope.targets.push({'code':'za', 'value':'ZipperAgent'});
		$scope.targets.push({'code':'zyprr', 'value':'Zyprr'});
		
		$scope.send = function(arg) {
			var messg = {
				'time': new Date().getTime(),
				'sent': true,
				'message': arg
			};
			
			$scope.messages.push(messg);
			
			$http.get('api/bot/query?q='+arg+'&s='+$scope.sessionId).then(function(data) {
				if (data.data.status === 200) {
					$scope.messages.push(data.data.result);
					$scope.sessionId = data.data.result.sessionId;
				}
			}, function() {
				
			});
		};
		
		$scope.submit = function(type, params) {
			if (type === 'calendar.event.add') {
				$http.post('api/bot/calendar/add', params).then(function(data) {
					if (data.data.status === 200) {
					}
				}, function() {
					
				});
			}
		};
		
		$scope.timeDiff = function(arg) {
			var now = new Date().getTime();
			var diff = now - arg;
			var retVal = "Just Now";
			var unit = "";
			diff = parseInt(diff/1000);
			
			if (diff > 60) {
				diff = diff/60;
				retVal = parseInt(diff) + "Mins. ago";
			}
			
			if (diff > 60) {
				diff = diff/60;
				retVal = parseInt(diff) + "Hours ago";
			}

			if (diff > 24) {
				diff = diff/24;
				retVal = parseInt(diff) + "Days ago";
			}
			
			return retVal + unit;
		};
		
		$scope.$on('chat', function() {
			$http.get('api/bot/hi').then(function(data) {
				if (data.data.status === 200) {
					$scope.messages.push(data.data.result);
					$scope.sessionId = data.data.result.sessionId;
				}
			}, function() {
				
			});
		});
	}]);
}(angular,$));